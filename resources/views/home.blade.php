@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <!-- .site-header-mobile -->
    <div id="main" class="site-main">
        <div class="container">
            <div class="row">
                <main id="site-content" class="col-md-12 col-xs-12 site-content">
                    <div class="site-content-inner">


                        <div class="page-content">
                            <div class="not-active-fullpage">
                                <div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <h1 style="text-align: left" class="vc_custom_heading hidden" ><a href="https://la-studioweb.com/go/airi/link">Airi - Multipurpose WooCommerce Theme</a></h1><div class="wpb_revslider_element wpb_content_element"><link href="https://fonts.googleapis.com/css?family=Montserrat:400%2C700%7CPlayfair+Display:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">

                                                    <section class="dart-no-padding-tb" id="home">
                                                    <div class="onImages">
                                                        <div class="ms-fullscreen-template" id="slider1-wrapper">

                                                            <div class="master-slider ms-skin-default" id="masterslider">
                                                                <div class="ms-slide slide-1">
                                                                    <img src="{{URL::asset('assets/masterslider/style/blank.gif')}}" data-src="{{URL::asset('assets/images/background@2x.jpg')}}" alt="lorem ipsum dolor sit">
                                                                    <h3 class="ms-layer myright" data-type="text" data-effect="back(1300)"
                                                                        data-duration="1800"
                                                                        data-delay="0"
                                                                        data-hide-effect="left(short,false)">
                                                                        AUCTUS LEGAL
                                                                    </h3>
                                                                    <h4 class="ms-layer mysecondright" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint">
                                                                        Best Solution Of Law
                                                                    </h4>
                                                                </div>
                                                                <div class="ms-slide slide-2">
                                                                    <h3 class="ms-layer myright" data-type="text" data-effect="top(45)" data-duration="3400" data-ease="easeOutExpo">
                                                                        WE CARE ABOUT
                                                                    </h3>
                                                                    <h4 class="ms-layer mysecondright" data-type="text" data-effect="top(45)" data-duration="3400" data-delay="400" data-ease="easeOutExpo">
                                                                        MEETING THE BUSINESS NEEDS<br> OF EVERY CLIENT
                                                                    </h4>
                                                                    <img src="{{URL::asset('assets/masterslider/style/blank.gif')}}" data-src="{{URL::asset('assets/images/background@2x.jpg')}}" alt="">
                                                                </div>
                                                                <div class="ms-slide slide-3">
                                                                    <h3 class="ms-layer myright" data-type="text" data-effect="top(45)" data-duration="3400" data-ease="easeOutExpo">
                                                                        WE CARE ABOUT
                                                                    </h3>
                                                                    <h4 class="ms-layer mysecondright" data-type="text" data-effect="top(45)" data-duration="3400" data-delay="400" data-ease="easeOutExpo">
                                                                        MEETING THE BUSINESS NEEDS<br> OF EVERY CLIENT
                                                                    </h4>
                                                                    <img src="{{URL::asset('assets/masterslider/style/blank.gif')}}" data-src="{{URL::asset('assets/images/background@2x.jpg')}}" alt="">
                                                                </div>
                                                            </div>

                                                        </div>
                                                        </section>
                                                    </div><!-- END REVOLUTION SLIDER -->

                                                </div><div id="la_divider5bb556afca135" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afca135'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:115px;sm:50px;&quot;}' ></div>
                                            </div></div></div>
                                </div>
                                <div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb556afcb73e" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afcb73e'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:70px;sm:20px;&quot;}' ></div>

                                                <div  class="wpb_single_image wpb_content_element vc_align_left   image_max_width margin-bottom-0">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="185" height="168" src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}" class="vc_single_image-img attachment-full" alt="" /></div>
                                                    </figure>
                                                </div>
                                                <div id="la_divider5bb556afcc6d3" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afcc6d3'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:30px;&quot;}' ></div>
                                                <div id="la_heading_5bb556afcc9b7" class="la-headings text-left"><div class="js-el subheading-tag la-unit-responsive three-font-family" style="color:#282828"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556afcc9b7 .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:16px;&quot;,&quot;line-height&quot;:&quot;lg:24px;&quot;}' >
                                                        <p>
                                                            At Fegor Shoes, we never settle. We have an always testing, ‘always in beta’ philosophy, constantly improving to make it all just that bit better every day. From free delivery and returns to innovative visual search tech, if it hasn’t been done before, we find a way to do it anyway.
                                                        </p>
                                                    </div></div>
                                                <div id="la_divider5bb556afccb35" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afccb35'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:30px;sm:10px;&quot;}' ></div>
                                                <p style="text-align: left" class="vc_custom_heading heading__button" ><a href="{{ route('shop') }}">Shop Now</a></p></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb556afcd1d3" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afcd1d3'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;xs:30px;&quot;}' ></div>

                                                <div style="margin-top: 130px"  class="wpb_single_image wpb_content_element vc_align_center   size-full la-popup image-with-icon-play icon-play-at-bottom">

                                                    <figure class="wpb_wrapper vc_figure">

                                                            <img width="570" height="560" src="{{URL::asset('assets/images/IMG_1414.JPG')}}" class="vc_single_image-img attachment-full" alt="" />

                                                    </figure>
                                                </div>
                                            </div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid la_row_gap_80 la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb556afcdc99" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afcdc99'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:60px;sm:30px;&quot;}' ></div>
                                                <div id="la_heading_5bb556afcde20" class="la-headings text-center"><h2 class="js-el heading-tag la-unit-responsive" style="" >Trending</h2></div>
                                                <div id="la_divider5bb556afce2a0" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556afce2a0'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:45px;sm:15px;&quot;}' ></div>
                                                <div id="recent_products_5bb556afce7d1" class="woocommerce products_scenario_recent_products">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <ul class="products grid-space-20 grid-items xxl-block-grid-4 xl-block-grid-4 lg-block-grid-3 md-block-grid-2 sm-block-grid-2 block-grid-1 products-grid products-grid-1">
                                                                <li class="product_item grid-item product post-897 type-product status-publish has-post-thumbnail product_cat-demo-02-sale-off product_cat-demo-02-top-sale product_cat-fashions product_tag-fashion first instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1409.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1409.JPG')}}"style="background-image: url({{URL::asset('assets/images/IMG_1423.JPG')}});"></div><div class="item--overlay"></div></a>			</div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto">
                                                                                    <a class="quickview button la-quickview-button" href="product/product-name-20/index.html" data-href="https://airi.la-studioweb.com/product/product-name-20/?product_quickview=897" title="Quick Shop">Quick Shop</a>
                                                                                    <a href="index50f7.html?add-to-cart=897" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="897" data-product_sku="REF. LA-897" aria-label="Add &ldquo;Limited edition v-neck t-shirt&rdquo; to your cart" rel="nofollow" data-product_title="Limited edition v-neck t-shirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="{{ route('shop') }}">Dapper man Leather Shoe</a></h3>
                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>15,500</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="{{ route('shop') }}" data-href="https://airi.la-studioweb.com/product/product-name-20/?product_quickview=897" title="Quick Shop">Quick Shop</a><a href="index50f7.html?add-to-cart=897" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="897" data-product_sku="REF. LA-897" aria-label="Add &ldquo;Limited edition v-neck t-shirt&rdquo; to your cart" rel="nofollow" data-product_title="Limited edition v-neck t-shirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-892 type-product status-publish has-post-thumbnail product_cat-demo-02-new-arrival product_cat-fashions product_tag-dress product_tag-fashion product_tag-women instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                    <span class="la-custom-badge odd" style="background-color:#282828;color:#ffffff"><span>New</span></span>
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1424.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1417.JPG')}}"style="background-image: url({{URL::asset('assets/images/IMG_1417.JPG')}});"></div>
                                                                                    <div class="item--overlay"></div>
                                                                                </a>
                                                                            </div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/hig-rise-skinny-jean/index.html" data-href="https://airi.la-studioweb.com/product/hig-rise-skinny-jean/?product_quickview=892" title="Quick Shop">Quick Shop</a><a href="index42c7.html?add-to-cart=892" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="892" data-product_sku="254" aria-label="Add &ldquo;Hig-Rise Skinny Jean&rdquo; to your cart" rel="nofollow" data-product_title="Hig-Rise Skinny Jean"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="product/hig-rise-skinny-jean/index.html">Pale Brown Patterned Tassels Loafers</a></h3>

                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>19,900</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/hig-rise-skinny-jean/index.html" data-href="https://airi.la-studioweb.com/product/hig-rise-skinny-jean/?product_quickview=892" title="Quick Shop">Quick Shop</a><a href="index42c7.html?add-to-cart=892" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="892" data-product_sku="254" aria-label="Add &ldquo;Hig-Rise Skinny Jean&rdquo; to your cart" rel="nofollow" data-product_title="Hig-Rise Skinny Jean"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-887 type-product status-publish has-post-thumbnail product_cat-demo-02-new-arrival product_cat-fashions instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><span class="la-custom-badge odd" style="background-color:#cf987e;color:#ffffff"><span>SALE</span></span>
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1410.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1418.JPG')}}" style="background-image: url({{URL::asset('assets/images/IMG_1418.JPG')}});"></div><div class="item--overlay"></div></a>			</div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-18/index.html" data-href="https://airi.la-studioweb.com/product/product-name-18/?product_quickview=887" title="Quick Shop">Quick Shop</a><a href="indexad93.html?add-to-cart=887" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="887" data-product_sku="REF. LA-887" aria-label="Add &ldquo;Waxed-effect pleated skirt&rdquo; to your cart" rel="nofollow" data-product_title="Waxed-effect pleated skirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="product/product-name-18/index.html">Fegor Monk SHoes in Blue</a></h3>
                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>19,900</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-18/index.html" data-href="https://airi.la-studioweb.com/product/product-name-18/?product_quickview=887" title="Quick Shop">Quick Shop</a><a href="indexad93.html?add-to-cart=887" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="887" data-product_sku="REF. LA-887" aria-label="Add &ldquo;Waxed-effect pleated skirt&rdquo; to your cart" rel="nofollow" data-product_title="Waxed-effect pleated skirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-882 type-product status-publish has-post-thumbnail product_cat-demo-02-sale-off product_cat-demo-02-top-sale product_cat-fashions last instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><span class="la-custom-badge odd" style="background-color:#d0021b;color:#ffffff"><span>Hot</span></span>
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1415.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1416.JPG')}}"style="background-image: url({{URL::asset('assets/images/IMG_1416.JPG')}});"></div><div class="item--overlay"></div></a>			</div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-17/index.html" data-href="https://airi.la-studioweb.com/product/product-name-17/?product_quickview=882" title="Quick Shop">Quick Shop</a><a href="index7d4d.html?add-to-cart=882" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="882" data-product_sku="REF. LA-882" aria-label="Add &ldquo;Chain print bermuda shorts&rdquo; to your cart" rel="nofollow" data-product_title="Chain print bermuda shorts"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Chain print bermuda shorts" data-product_id="882">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Chain print bermuda shorts" data-product_id="882">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="product/product-name-17/index.html">Fegorshoes vegan tassels loafers in black patent</a></h3>
                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>19,900</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-17/index.html" data-href="https://airi.la-studioweb.com/product/product-name-17/?product_quickview=882" title="Quick Shop">Quick Shop</a><a href="index7d4d.html?add-to-cart=882" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="882" data-product_sku="REF. LA-882" aria-label="Add &ldquo;Chain print bermuda shorts&rdquo; to your cart" rel="nofollow" data-product_title="Chain print bermuda shorts"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Chain print bermuda shorts" data-product_id="882">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Chain print bermuda shorts" data-product_id="882">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-876 type-product status-publish has-post-thumbnail product_cat-demo-02-new-arrival product_cat-fashions first instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1420.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1422.JPG')}}" style="background-image: url({{URL::asset('assets/images/IMG_1422.JPG')}});"></div><div class="item--overlay"></div></a>			</div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-16/index.html" data-href="https://airi.la-studioweb.com/product/product-name-16/?product_quickview=876" title="Quick Shop">Quick Shop</a><a href="indexdfcd.html?add-to-cart=876" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="876" data-product_sku="REF. LA-876" aria-label="Add &ldquo;Check blazer&rdquo; to your cart" rel="nofollow" data-product_title="Check blazer"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Check blazer" data-product_id="876">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Check blazer" data-product_id="876">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="product/product-name-16/index.html">Check blazer</a></h3>
                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>19,900</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-16/index.html" data-href="https://airi.la-studioweb.com/product/product-name-16/?product_quickview=876" title="Quick Shop">Quick Shop</a><a href="indexdfcd.html?add-to-cart=876" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="876" data-product_sku="REF. LA-876" aria-label="Add &ldquo;Check blazer&rdquo; to your cart" rel="nofollow" data-product_title="Check blazer"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Check blazer" data-product_id="876">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Check blazer" data-product_id="876">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-897 type-product status-publish has-post-thumbnail product_cat-demo-02-sale-off product_cat-demo-02-top-sale product_cat-fashions product_tag-fashion first instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1409.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1409.JPG')}}"style="background-image: url({{URL::asset('assets/images/IMG_1423.JPG')}});"></div><div class="item--overlay"></div></a>			</div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto">
                                                                                    <a class="quickview button la-quickview-button" href="product/product-name-20/index.html" data-href="https://airi.la-studioweb.com/product/product-name-20/?product_quickview=897" title="Quick Shop">Quick Shop</a>
                                                                                    <a href="index50f7.html?add-to-cart=897" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="897" data-product_sku="REF. LA-897" aria-label="Add &ldquo;Limited edition v-neck t-shirt&rdquo; to your cart" rel="nofollow" data-product_title="Limited edition v-neck t-shirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="{{ route('shop') }}">Dapper man Leather Shoe</a></h3>
                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>15,500</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="{{ route('shop') }}" data-href="https://airi.la-studioweb.com/product/product-name-20/?product_quickview=897" title="Quick Shop">Quick Shop</a><a href="index50f7.html?add-to-cart=897" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="897" data-product_sku="REF. LA-897" aria-label="Add &ldquo;Limited edition v-neck t-shirt&rdquo; to your cart" rel="nofollow" data-product_title="Limited edition v-neck t-shirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Limited edition v-neck t-shirt" data-product_id="897">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-892 type-product status-publish has-post-thumbnail product_cat-demo-02-new-arrival product_cat-fashions product_tag-dress product_tag-fashion product_tag-women instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                    <span class="la-custom-badge odd" style="background-color:#282828;color:#ffffff"><span>New</span></span>
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1424.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1417.JPG')}}"style="background-image: url({{URL::asset('assets/images/IMG_1417.JPG')}});"></div>
                                                                                    <div class="item--overlay"></div>
                                                                                </a>
                                                                            </div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/hig-rise-skinny-jean/index.html" data-href="https://airi.la-studioweb.com/product/hig-rise-skinny-jean/?product_quickview=892" title="Quick Shop">Quick Shop</a><a href="index42c7.html?add-to-cart=892" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="892" data-product_sku="254" aria-label="Add &ldquo;Hig-Rise Skinny Jean&rdquo; to your cart" rel="nofollow" data-product_title="Hig-Rise Skinny Jean"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="product/hig-rise-skinny-jean/index.html">Pale Brown Patterned Tassels Loafers</a></h3>

                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>19,900</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/hig-rise-skinny-jean/index.html" data-href="https://airi.la-studioweb.com/product/hig-rise-skinny-jean/?product_quickview=892" title="Quick Shop">Quick Shop</a><a href="index42c7.html?add-to-cart=892" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="892" data-product_sku="254" aria-label="Add &ldquo;Hig-Rise Skinny Jean&rdquo; to your cart" rel="nofollow" data-product_title="Hig-Rise Skinny Jean"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Hig-Rise Skinny Jean" data-product_id="892">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-887 type-product status-publish has-post-thumbnail product_cat-demo-02-new-arrival product_cat-fashions instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                                    <div class="product_item--inner">
                                                                        <div class="product_item--thumbnail">
                                                                            <div class="product_item--thumbnail-holder">
                                                                                <a href="{{ route('shop') }}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><span class="la-custom-badge odd" style="background-color:#cf987e;color:#ffffff"><span>SALE</span></span>
                                                                                    <img width="500" height="575" src="{{URL::asset('assets/images/IMG_1410.JPG')}}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                                    <div class="wp-alt-image" data-background-image="{{URL::asset('assets/images/IMG_1418.JPG')}}" style="background-image: url({{URL::asset('assets/images/IMG_1418.JPG')}});"></div><div class="item--overlay"></div></a>			</div>
                                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-18/index.html" data-href="https://airi.la-studioweb.com/product/product-name-18/?product_quickview=887" title="Quick Shop">Quick Shop</a><a href="indexad93.html?add-to-cart=887" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="887" data-product_sku="REF. LA-887" aria-label="Add &ldquo;Waxed-effect pleated skirt&rdquo; to your cart" rel="nofollow" data-product_title="Waxed-effect pleated skirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                        <div class="product_item--info">
                                                                            <div class="product_item--info-inner">
                                                                                <h3 class="product_item--title"><a href="product/product-name-18/index.html">Fegor Monk SHoes in Blue</a></h3>
                                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>19,900</span></span>
                                                                            </div>
                                                                            <div class="product_item--action">
                                                                                <div class="wrap-addto"><a class="quickview button la-quickview-button" href="product/product-name-18/index.html" data-href="https://airi.la-studioweb.com/product/product-name-18/?product_quickview=887" title="Quick Shop">Quick Shop</a><a href="indexad93.html?add-to-cart=887" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="887" data-product_sku="REF. LA-887" aria-label="Add &ldquo;Waxed-effect pleated skirt&rdquo; to your cart" rel="nofollow" data-product_title="Waxed-effect pleated skirt"><span title="Add to cart">Add to cart</span></a><a class="add_wishlist button la-core-wishlist" href="#" title="Add to Wishlist" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Wishlist</a><a class="add_compare button la-core-compare" href="#" title="Add to Compare" rel="nofollow" data-product_title="Waxed-effect pleated skirt" data-product_id="887">Add to Compare</a></div>			</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="product_item grid-item product post-882 type-product status-publish has-post-thumbnail product_cat-demo-02-sale-off product_cat-demo-02-top-sale product_cat-fashions last instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            <div id="la_divider5bb556b012825" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b012825'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:45px;sm:15px;&quot;}' ></div>
                                                <p style="text-align: center" class="vc_custom_heading heading__button" ><a href="{{route('shop')}}">View All</a></p><div id="la_divider5bb556b0144ae" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b0144ae'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:50px;sm:40px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1535617477760 vc_row-has-fill la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1535617632712"><div class="wpb_wrapper"><div id="la_divider5bb556b014a91" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b014a91'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:50px;sm:20px;&quot;}' ></div>
                                                <div id="la_carousel_6894649515bb556b014bfe" class="la-carousel-wrapper la_carousel_horizontal slider-item-middle slider-image-overlay" data-gutter="15">
                                                    <div data-la_component="AutoCarousel" class="js-el la-slick-slider" data-slider_config="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6,&quot;dots&quot;:false,&quot;autoplay&quot;:true,&quot;autoplaySpeed&quot;:5000,&quot;speed&quot;:1000,&quot;infinite&quot;:true,&quot;arrows&quot;:false,&quot;swipe&quot;:true,&quot;draggable&quot;:true,&quot;touchMove&quot;:true,&quot;pauseOnHover&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1824,&quot;settings&quot;:{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:576,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}}],&quot;pauseOnDotsHover&quot;:true}">
                                                        <div class="la-item-wrap">
                                                            <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="110" height="102" src="https://i1.wp.com/airi.la-studioweb.com/wp-content/uploads/2018/08/logo-partner1.png?fit=110%2C102&amp;zoom=2&amp;ssl=1" class="vc_single_image-img attachment-full" alt="" /></div>
                                                                </figure>
                                                            </div>
                                                        </div><div class="la-item-wrap">
                                                            <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="75" height="73" src="https://i0.wp.com/airi.la-studioweb.com/wp-content/uploads/2018/08/logo-partner2.png?fit=75%2C73&amp;zoom=2&amp;ssl=1" class="vc_single_image-img attachment-full" alt="" /></div>
                                                                </figure>
                                                            </div>
                                                        </div><div class="la-item-wrap">
                                                            <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="91" height="90" src="https://i0.wp.com/airi.la-studioweb.com/wp-content/uploads/2018/08/logo-partner3.png?fit=91%2C90&amp;zoom=2&amp;ssl=1" class="vc_single_image-img attachment-full" alt="" /></div>
                                                                </figure>
                                                            </div>
                                                        </div><div class="la-item-wrap">
                                                            <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="108" height="87" src="https://i1.wp.com/airi.la-studioweb.com/wp-content/uploads/2018/08/logo-partner4.png?fit=108%2C87&amp;zoom=2&amp;ssl=1" class="vc_single_image-img attachment-full" alt="" /></div>
                                                                </figure>
                                                            </div>
                                                        </div><div class="la-item-wrap">
                                                            <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="76" height="75" src="https://i0.wp.com/airi.la-studioweb.com/wp-content/uploads/2018/08/logo-partner5.png?fit=76%2C75&amp;zoom=2&amp;ssl=1" class="vc_single_image-img attachment-full" alt="" /></div>
                                                                </figure>
                                                            </div>
                                                        </div><div class="la-item-wrap">
                                                            <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                                <figure class="wpb_wrapper vc_figure">
                                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="91" height="108" src="https://i1.wp.com/airi.la-studioweb.com/wp-content/uploads/2018/08/logo-partner6.png?fit=91%2C108&amp;zoom=2&amp;ssl=1" class="vc_single_image-img attachment-full" alt="" /></div>
                                                                </figure>
                                                            </div>
                                                        </div>    </div>
                                                </div><style data-la_component="InsertCustomCSS">
                                                    #la_carousel_6894649515bb556b014bfe{
                                                        margin-left: -15px;
                                                        margin-right: -15px;
                                                    }
                                                    #la_carousel_6894649515bb556b014bfe .la-item-wrap.slick-slide{
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                    }
                                                </style>
                                                <div id="la_divider5bb556b01a9ff" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b01a9ff'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:50px;sm:20px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb556b01ad5d" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b01ad5d'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:25px;sm:15px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_heading_5bb556b01b0e6" class="la-headings text-left element-max-width-470"><h2 class="js-el heading-tag la-unit-responsive" style="" >Top Collections</h2>
                                                    <div class="js-el subheading-tag la-unit-responsive three-font-family padding-top-10" style="color:#282828"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b01b0e6 .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:16px;&quot;,&quot;line-height&quot;:&quot;lg:31px;&quot;}' >
                                                        <p>You know that satisfying feeling when you stumble across an incredible vintage boutique, or uncover an amazing independent brand, before everyone else? Yeah, we love that too – so you can shop one-of-a-kind finds all the time.</p>
                                                    </div></div>
                                                <div id="la_divider5bb556b01cf35" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b01cf35'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:35px;&quot;}' ></div>
                                                <p style="text-align: left" class="vc_custom_heading heading__button" ><a href="{{ route('shop') }}">View All</a></p></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_column-gap-20"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_banner_box_5bb556b01da05" class="wpb_content_element la-banner-box banner-type-4 la_banner_box_5bb556b01da05 margin-bottom-20">
                                                                    <div class="box-inner">
                                                                        <div class="banner--image">
                                                                            <div class="loop__item__thumbnail--bkg gitem-zone-height-mode-original"
                                                                                 data-background-image="{{URL::asset('assets/images/IMG_1411.JPG')}}"
                                                                                 style="padding-top:77.19%;background-image: url({{URL::asset('assets/images/IMG_1411.JPG')}});">
                                                                            </div>
                                                                        </div>
                                                                        <div class="banner--info">
                                                                            <div class="js-el b-title b-title1" style="" ><span>Shop</span> <span>Now</span></div><a class="banner--btn" href="{{ route('shop') }}" title="Shop Now">Shop Now</a>        </div>
                                                                        <a class="banner--link-overlay item--overlay" href="{{ route('shop') }}" title="Shop Now"><span class="hidden"><span>Shop Now</span></span></a>    </div>
                                                                </div><div id="la_banner_box_5bb556b020692" class="wpb_content_element la-banner-box banner-type-4 la_banner_box_5bb556b020692 margin-bottom-0">
                                                                    <div class="box-inner">
                                                                        <div class="banner--image">
                                                                            <div class="loop__item__thumbnail--bkg gitem-zone-height-mode-original"
                                                                                 data-background-image="{{URL::asset('assets/images/IMG_1415.JPG')}}"
                                                                                 style="padding-top:72.98%;background-image: url({{URL::asset('assets/images/IMG_1415.JPG')}});">
                                                                            </div>
                                                                        </div>
                                                                        <div class="banner--info">
                                                                            <div class="js-el b-title b-title1" style="" ><span>Shop</span> <span>Now</span></div><a class="banner--btn" href="{{ route('shop') }}" title="Shop Now">Shop Now</a>        </div>
                                                                        <a class="banner--link-overlay item--overlay" href="{{ route('shop') }}" title="Shop Now"><span class="hidden"><span>Shop Now</span></span></a>    </div>
                                                                </div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_banner_box_5bb556b020fec" class="wpb_content_element la-banner-box banner-type-4 la_banner_box_5bb556b020fec margin-bottom-0">
                                                                    <div class="box-inner">
                                                                        <div class="banner--image">
                                                                            <div class="loop__item__thumbnail--bkg gitem-zone-height-mode-original"
                                                                                 data-background-image="{{URL::asset('assets/images/IMG_1420.JPG')}}"
                                                                                 style="padding-top:157.19%;background-image: url({{URL::asset('assets/images/IMG_1420.JPG')}});">
                                                                            </div>
                                                                        </div>
                                                                        <div class="banner--info">
                                                                            <div class="js-el b-title b-title1" style="" ><span>Shop</span> <span>Now</span></div><a class="banner--btn" href="{{ route('shop') }}" title="Shop Now">Shop Now</a>        </div>
                                                                        <a class="banner--link-overlay item--overlay" href="{{ route('shop') }}" title="Shop Now"><span class="hidden"><span>Shop Now</span></span></a>    </div>
                                                                </div></div></div></div></div></div></div></div></div><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb556b021531" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b021531'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:60px;sm:30px;&quot;}' ></div>
                                            </div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1535618228226 vc_row-has-fill la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-8 vc_col-sm-offset-2"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb556b021a6c" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b021a6c'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:20px;&quot;}' ></div>
                                                <div id="la_heading_5bb556b021cbe" class="la-headings text-center"><h2 class="js-el heading-tag la-unit-responsive" style="" >Join Our Newsletter</h2><div class="js-el subheading-tag la-unit-responsive letter-spacing-2 padding-top-10 font-weight-600" style="color:#282828"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b021cbe .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:16px;&quot;,&quot;line-height&quot;:&quot;lg:31px;&quot;}' ><p>BE IN THE KNOW!</p>
                                                    </div></div>
                                                <div id="la_divider5bb556b022942" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b022942'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:5px;&quot;}' ></div>

                                                <section id="yikes-mailchimp-container-1" class="yikes-mailchimp-container yikes-mailchimp-container-1 easy_mc__style1 font-size-16">
                                                    <form id="airi-demo-1" class="yikes-easy-mc-form yikes-easy-mc-form-1  " method="POST" data-attr-form-id="1">

                                                        <label for="yikes-easy-mc-form-1-EMAIL"  class="EMAIL-label yikes-mailchimp-field-required ">

                                                            <!-- dictate label visibility -->

                                                            <!-- Description Above -->

                                                            <input id="yikes-easy-mc-form-1-EMAIL"  name="EMAIL"  placeholder="Enter your email address.."  class="yikes-easy-mc-email field-no-label"  required="required" type="email"  value="">

                                                            <!-- Description Below -->

                                                        </label>

                                                        <!-- Honeypot Trap -->
                                                        <input type="hidden" name="yikes-mailchimp-honeypot" id="yikes-mailchimp-honeypot-1" value="">

                                                        <!-- List ID -->
                                                        <input type="hidden" name="yikes-mailchimp-associated-list-id" id="yikes-mailchimp-associated-list-id-1" value="eea619a0ca">

                                                        <!-- The form that is being submitted! Used to display error/success messages above the correct form -->
                                                        <input type="hidden" name="yikes-mailchimp-submitted-form" id="yikes-mailchimp-submitted-form-1" value="1">

                                                        <!-- Submit Button -->
                                                        <button type="submit" class="yikes-easy-mc-submit-button yikes-easy-mc-submit-button-1 btn btn-primary "> <span class="yikes-mailchimp-submit-button-span-text">Subscribe</span></button>				<!-- Nonce Security Check -->
                                                        <input type="hidden" id="yikes_easy_mc_new_subscriber_1" name="yikes_easy_mc_new_subscriber" value="9d0da24223">
                                                        <input type="hidden" name="_wp_http_referer" value="/" />
                                                    </form>
                                                    <!-- MailChimp Form generated by Easy Forms for MailChimp v6.4.7 (https://wordpress.org/plugins/yikes-inc-easy-mailchimp-extender/) -->

                                                </section>
                                                <div id="la_divider5bb556b0238b1" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb556b0238b1'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:105px;sm:60px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>
                            </div>                    </div>

                    </div>
                </main>
                <!-- #site-content -->
            </div>
        </div>
    </div>
    <!-- .site-main -->
@endsection
