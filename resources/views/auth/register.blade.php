@extends('layouts.app')

@section('content')
    <style>
        #page.site {
            /*Uncomment later*/
            min-height: 300px;
            position: relative;
        }
    </style>
    <section id="section_page_header" class="wpb_row section-page-header">
        <div class="container">
            <div class="page-header-inner">
                <div class="row">
                    <div class="col-xs-12">
                        <header><h1 class="page-title">My Account</h1></header>
                        <div class="la-breadcrumbs"><div>
                                <div class="la-breadcrumb-content">
                                    <div class="la-breadcrumb-wrap"><div class="la-breadcrumb-item"><a href="../../index.html" class="la-breadcrumb-item-link is-home" rel="home" title="Home">Home</a></div>
                                        <div class="la-breadcrumb-item"><div class="la-breadcrumb-item-sep">/</div></div> <div class="la-breadcrumb-item"><span class="la-breadcrumb-item-target">My Account</span></div>
                                    </div>
                                </div></div>
                        </div>                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- #page_header -->
    <div id="main" class="site-main">
        <div class="container">
            <div class="row">
                <main id="site-content" class="col-md-12 col-xs-12 site-content">
                    <div class="site-content-inner">


                        <div class="page-content">
                            <div class="not-active-fullpage"><div class="woocommerce">



                                    <div class="u-columns col2-set" id="customer_login">

                                        <div class="u-column1 col-1">


                                            <h2>Login</h2>

                                            <form action="{{ route('login') }}" class="woocommerce-form woocommerce-form-login login" method="post">
                                                @csrf

                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="username">Email address&nbsp;<span class="required">*</span></label>
                                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" />
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>
                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="password">Password&nbsp;<span class="required">*</span></label>
                                                    <input class="woocommerce-Input woocommerce-Input--text input-text {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" id="password" />
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>


                                                <p class="form-row">
                                                    <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce" value="5d7e83525e" /><input type="hidden" name="_wp_http_referer" value="/my-account/edit-account/" />				<button type="submit" class="woocommerce-Button button" name="login" value="Log in">Log in</button>
                                                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                                        <input class="woocommerce-form__input woocommerce-form__input-checkbox " name="remember" type="checkbox" id="rememberme" value="forever" {{ old('remember') ? 'checked' : '' }} /> <span>Remember me</span>
                                                    </label>
                                                </p>
                                                <p class="woocommerce-LostPassword lost_password">
                                                    <a href="{{ route('password.request') }}">Lost your password?</a>
                                                </p>


                                            </form>


                                        </div>

                                        <div class="u-column2 col-2">

                                            <h2>Register</h2>

                                            <form method="post" action="{{ __('register') }}" class="woocommerce-form woocommerce-form-register register">
                                                @csrf

                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="reg_email">Full Name&nbsp;<span class="required">*</span></label>
                                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" />
                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>

                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="reg_email">Email address&nbsp;<span class="required">*</span></label>
                                                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="reg_email" autocomplete="email" value="{{ old('email') }}" />
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>


                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="reg_password">Password&nbsp;<span class="required">*</span></label>
                                                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="reg_password" autocomplete="new-password" />
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>

                                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                                    <label for="reg_password">Confirm Password&nbsp;<span class="required">*</span></label>
                                                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text " name="password_confirmation" />
                                                </p>


                                                <div class="woocommerce-privacy-policy-text"><p>Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our <a href="../../index3b72.html?page_id=3" class="woocommerce-privacy-policy-link" target="_blank">privacy policy</a>.</p>
                                                </div>
                                                <p class="woocommerce-FormRow form-row">
                                                    <button type="submit" class="woocommerce-Button button" value="Register">{{ __('Register') }}</button>
                                                </p>


                                            </form>

                                        </div>

                                    </div>

                                </div>
                            </div>                    </div>

                    </div>
                </main>
                <!-- #site-content -->
            </div>
        </div>
    </div>
@endsection
