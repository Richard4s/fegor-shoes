<script src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{URL::asset('assets/js/modernizr.min.js')}}"></script>
<script src="{{URL::asset('assets/js/detect.js')}}"></script>
<script src="{{URL::asset('assets/js/fastclick.js')}}"></script>
<script src="{{URL::asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{URL::asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{URL::asset('assets/js/waves.js')}}"></script><!-- skycons -->
<script src="{{URL::asset('assets/plugins/skycons/skycons.min.js')}}"></script><!-- skycons -->
<script src="{{URL::asset('assets/plugins/peity/jquery.peity.min.js')}}"></script><!--Morris Chart-->
<script src="{{URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script><!-- dashboard -->
<script src="{{URL::asset('assets/pages/dashboard.js')}}"></script><!-- App js -->
<script src="{{URL::asset('assets/js/waves.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script><!-- Buttons examples -->
<script src="{{URL::asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/buttons.colVis.min.js')}}"></script><!-- Responsive examples -->
<script src="{{URL::asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
<script src="{{URL::asset('assets/pages/sweet-alert.init.js')}}"></script><!-- App js -->
<script src="{{URL::asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script><!-- Datatable init js -->
<script src="{{URL::asset('assets/pages/datatables.init.js')}}"></script><!-- App js -->
<script src="{{URL::asset('assets/plugins/dropzone/dist/dropzone.js')}}"></script><!-- App js -->
<script src="{{URL::asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script><!-- App js -->
<script src="{{URL::asset('assets/js/dropify.js')}}"></script>
<script src="{{URL::asset('assets/js/custom.js')}}"></script><!-- App js -->
<script src="{{URL::asset('assets/js/app.js')}}"></script>


<script>

    jQuery(document).ready(function () {
        $('.dropify').dropify();
        $('.summernote').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                 // set focus to editable area after initializing summernote
        });
    });
</script>

</body>
</html>