<footer id="colophon" class="site-footer la-footer-5col32223">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="footer-column footer-column-1 col-xs-12 col-sm-6 col-md-3"><div class="footer-column-inner"><div id="text-9" class="margin-bottom-10 widget widget_text">			<div class="textwidget"><p class="margin-bottom-10"><img class="alignnone wp-image-1032" src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}" alt="" width="150" height="48" data-recalc-dims="1" /></p>

                            </div>
                        </div><div id="text-10" class="font-size-18 text-color-white widget widget_text">			<div class="textwidget">
                                <div class="social-media-link style-default">
                                    <a href="#" class="facebook" title="Facebook" target="_blank" rel="nofollow">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="instagram.com/fegorshoes" class="twitter" title="Instagram" target="_blank" rel="nofollow">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </div>
                            </div>
                        </div></div></div><div class="footer-column footer-column-2 col-xs-12 col-sm-3 col-md-2"><div class="footer-column-inner"><div id="nav_menu-3" class="widget widget_nav_menu"><div class="widget-inner"><h3 class="widget-title"><span>Company</span></h3><div class="menu-footer-company-container"><ul id="menu-footer-company" class="menu"><li id="menu-item-622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-622"><a href="#">About Us</a></li>
                                        <li id="menu-item-623" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-623"><a href="#">Our Services</a></li>
                                        <li id="menu-item-624" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-624"><a href="#">Affiliate Program</a></li>
                                        <li id="menu-item-625" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-625"><a href="#">Work for Fegorshoes</a></li>
                                    </ul></div></div></div></div></div><div class="footer-column footer-column-3 col-xs-12 col-sm-3 col-md-2"><div class="footer-column-inner"><div id="nav_menu-2" class="widget widget_nav_menu"><div class="widget-inner"><h3 class="widget-title"><span>Useful Links</span></h3><div class="menu-footer-useful-links-container"><ul id="menu-footer-useful-links" class="menu"><li id="menu-item-626" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-626"><a href="#">The Collections</a></li>
                                        <li id="menu-item-627" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-627"><a href="#">Size Guide</a></li>
                                        <li id="menu-item-628" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-628"><a href="#">Return Policy</a></li>
                                    </ul></div></div></div></div></div><div class="footer-column footer-column-4 col-xs-12 col-sm-6 col-md-2"><div class="footer-column-inner"><div id="nav_menu-4" class="widget widget_nav_menu"><div class="widget-inner"><h3 class="widget-title"><span>Shopping</span></h3><div class="menu-footer-shopping-container"><ul id="menu-footer-shopping" class="menu"><li id="menu-item-629" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-629"><a href="#">Look Book</a></li>
                                        <li id="menu-item-630" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-630"><a href="#">Shop Leftsidebar</a></li>
                                        <li id="menu-item-631" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-631"><a href="#">Shop Masonry</a></li>
                                        <li id="menu-item-632" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-632"><a href="#">Man &#038; Woman</a></li>
                                    </ul></div></div></div></div></div><div class="footer-column footer-column-5 col-xs-12 col-sm-6 col-md-3"><div class="footer-column-inner"><div id="contact_info-2" class="margin-bottom-0 widget widget_contact_info"><h3 class="widget-title"><span>Contact Info</span></h3><div class="la-contact-info"><div class="la-contact-item la-contact-phone"><span><a href="tel:+2349039464953">(+234) 903 943 6495</a></span></div>
                                <div class="la-contact-item la-contact-email"><span><a href="mailto:fegor.ruadjere@gmail.com">fegor.ruadjere@gmail.com</a></span></div><div class="la-contact-item la-contact-address"><span>Lekki, Lagos.</span></div></div></div><div id="text-11" class="widget widget_text">			<div class="textwidget"><p><img src="https://i1.wp.com/veera.la-studioweb.com/wp-content/themes/veera/assets/images/payments.png?w=1170&amp;zoom=2&amp;ssl=1" alt="payment" data-recalc-dims="1" /></p>
                            </div>
                        </div></div></div>            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-inner">

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div id="la_heading_5bb556b040daa" class="la-headings text-left"><div class="js-el heading-tag la-unit-responsive letter-spacing-1 font-weight-500 padding-bottom-5" style="color:#d0d0d0"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b040daa .heading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >FREE DELIVERY NATION WIDE</div><div class="js-el subheading-tag la-unit-responsive" style="color:#949494"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b040daa .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' ><p>Free Delivery over order ₦100,000</p>
                            </div></div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div id="la_heading_5bb556b040ee9" class="la-headings text-left"><div class="js-el heading-tag la-unit-responsive letter-spacing-1 font-weight-500 padding-bottom-5" style="color:#d0d0d0"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b040ee9 .heading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >30 DAYS MONEY BACK</div><div class="js-el subheading-tag la-unit-responsive" style="color:#949494"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b040ee9 .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' ><p>You can back money any times</p>
                            </div></div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div id="la_heading_5bb556b040ff2" class="la-headings text-left"><div class="js-el heading-tag la-unit-responsive letter-spacing-1 font-weight-500 padding-bottom-5" style="color:#d0d0d0"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b040ff2 .heading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >PROFESSIONAL SUPPORT 24/7</div><div class="js-el subheading-tag la-unit-responsive" style="color:#949494"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b040ff2 .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' ><p>fegor.ruadjere@gmail.com</p>
                            </div></div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div id="la_heading_5bb556b041107" class="la-headings text-left"><div class="js-el heading-tag la-unit-responsive letter-spacing-1 font-weight-500 padding-bottom-5" style="color:#d0d0d0"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b041107 .heading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >100% SECURE CHECKOUT</div><div class="js-el subheading-tag la-unit-responsive" style="color:#949494"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb556b041107 .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:12px;&quot;,&quot;line-height&quot;:&quot;&quot;}' ><p>Protect buyer & clients</p>
                            </div></div>

                    </div>
                </div>
                <div class="row font-size-11 padding-top-20 padding-bottom-5">
                    <div class="col-xs-12 text-center">
                        © 2018 Fegorshoes All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- #colophon --></div><!-- .site-inner --></div><!-- #page-->
<div class="searchform-fly-overlay">
    <a href="javascript:;" class="btn-close-search"><i class="dl-icon-close"></i></a>
    <div class="searchform-fly">
        <p>Start typing and press Enter to search</p>
        <form method="get" class="search-form" action="https://airi.la-studioweb.com/">
            <input autocomplete="off" type="search" class="search-field" placeholder="Search entire store&hellip;" value="" name="s" title="Search for:" />
            <button class="search-button" type="submit"><i class="dl-icon-search10"></i></button>
            <input type="hidden" name="post_type" value="product" />
        </form>
        <!-- .search-form -->        <div class="search-results">
            <div class="loading"><div class="la-loader spinner3"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>
            <div class="results-container"></div>
            <div class="view-more-results text-center">
                <a href="#" class="button search-results-button">View more</a>
            </div>
        </div>
    </div>
</div>
<!-- .search-form -->

<div class="cart-flyout">
    <div class="cart-flyout--inner">
        <a href="javascript:;" class="btn-close-cart"><i class="dl-icon-close"></i></a>
        <div class="cart-flyout__content">
            <div class="cart-flyout__heading">Shopping Cart</div>
            <div class="cart-flyout__loading"><div class="la-loader spinner3"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>
            <div class="widget_shopping_cart_content">

                <p class="woocommerce-mini-cart__empty-message">No products in the cart.</p>


            </div>
        </div>
    </div>
</div>
<div class="la-overlay-global"></div>

<div data-la_component="NewsletterPopup" class="js-el la-newsletter-popup" data-waitfortrigger="0" data-back-time="1" data-show-mobile="1" id="la_newsletter_popup" data-delay="2000">
    <a href="#" class="btn-close-newsletter-popup"><i class="dl-icon-close"></i></a>
    <div class="newsletter-popup-content">
        <h4>Join our newsletter</h4>

        <section id="yikes-mailchimp-container-1" class="yikes-mailchimp-container yikes-mailchimp-container-1 ">
            <form id="airi-demo-1" class="yikes-easy-mc-form yikes-easy-mc-form-1  " method="POST" data-attr-form-id="1">

                <label for="yikes-easy-mc-form-1-EMAIL"  class="EMAIL-label yikes-mailchimp-field-required ">

                    <!-- dictate label visibility -->

                    <!-- Description Above -->

                    <input id="yikes-easy-mc-form-1-EMAIL"  name="EMAIL"  placeholder="Enter your email address.."  class="yikes-easy-mc-email field-no-label"  required="required" type="email"  value="">

                    <!-- Description Below -->

                </label>

                <!-- Honeypot Trap -->
                <input type="hidden" name="yikes-mailchimp-honeypot" id="yikes-mailchimp-honeypot-1" value="">

                <!-- List ID -->
                <input type="hidden" name="yikes-mailchimp-associated-list-id" id="yikes-mailchimp-associated-list-id-1" value="eea619a0ca">

                <!-- The form that is being submitted! Used to display error/success messages above the correct form -->
                <input type="hidden" name="yikes-mailchimp-submitted-form" id="yikes-mailchimp-submitted-form-1" value="1">

                <!-- Submit Button -->
                <button type="submit" class="yikes-easy-mc-submit-button yikes-easy-mc-submit-button-1 btn btn-primary "> <span class="yikes-mailchimp-submit-button-span-text">Submit</span></button>				<!-- Nonce Security Check -->
                <input type="hidden" id="yikes_easy_mc_new_subscriber_1" name="yikes_easy_mc_new_subscriber" value="9d0da24223">
                <input type="hidden" name="_wp_http_referer" value="/" />
            </form>
            <!-- MailChimp Form generated by Easy Forms for MailChimp v6.4.7 (https://wordpress.org/plugins/yikes-inc-easy-mailchimp-extender/) -->

        </section>
    </div>
    <label class="lbl-dont-show-popup"><input type="checkbox" class="cbo-dont-show-popup" id="dont_show_popup"/>Do not show popup anymore</label>
</div>
<script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
</script>
<link rel='stylesheet' id='yikes-inc-easy-mailchimp-public-styles-css'  href='{{URL::asset('assets/wp-content/plugins/yikes-inc-easy-mailchimp-extender/public/css/yikes-inc-easy-mailchimp-extender-public.min5010.css')}}'  media='all' />
<script src='{{URL::asset('assets/wp-content/plugins/jetpack/_inc/build/photon/photon.minb3d9.js')}}'></script>


<script src='{{URL::asset('assets/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js')}}' defer ></script>
<script src='{{URL::asset('assets/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js')}}' defer ></script>

<script src='{{URL::asset('assets/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min4c8b.js')}}' defer ></script>

<script>
    jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
        jQuery( 'body' ).trigger( 'jetpack-lazy-images-load' );
    } );

</script>
<script src='{{URL::asset('assets/wp-content/themes/airi/assets/js/enqueue/min/modernizr-custom.js')}}' defer ></script>
<script src='{{URL::asset('assets/wp-content/themes/airi/assets/js/plugins/min/plugins-full.js')}}' defer ></script>
<script src="{{URL::asset('assets/masterslider/jquery.easing.min.js')}}"></script>
<script src="{{URL::asset('assets/masterslider/masterslider.min.js')}}"></script>

<script type="text/javascript">

    var slider = new MasterSlider();

    slider.control('bullets');

    slider.setup('masterslider' , {
        width:1024,
        height:700,
        space:5,
        view:'fade',
        layout:'partialheight',
        speed:20,
        overPause:false,
        autoplay:true
    });

</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var la_theme_config = {"security":{"favorite_posts":"43e9b2991d","wishlist_nonce":"b2d41b4755","compare_nonce":"5ccbb4c8bb","instagram_token":"8205164261.3a81a9f.ba755ce4da85464b8a9a3e15a15c7db4"},"fullpage":[],"product_single_design":"2","product_gallery_column":"{\"xlg\":\"3\",\"lg\":\"3\",\"md\":\"3\",\"sm\":\"5\",\"xs\":\"4\",\"mb\":\"3\"}","single_ajax_add_cart":"yes","i18n":{"backtext":"Back","compare":{"view":"View List Compare","success":"has been added to comparison list.","error":"An error occurred ,Please try again !"},"wishlist":{"view":"View List Wishlist","success":"has been added to your wishlist.","error":"An error occurred, Please try again !"},"addcart":{"view":"View Cart","success":"has been added to your cart","error":"An error occurred, Please try again !"},"global":{"error":"An error occurred ,Please try again !","comment_author":"Please enter Name !","comment_email":"Please enter Email Address !","comment_rating":"Please select a rating !","comment_content":"Please enter Comment !","continue_shopping":"Continue Shopping","cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser"}},"popup":{"max_width":"1070","max_height":"460"},"js_path":"https:\/\/airi.la-studioweb.com\/wp-content\/themes\/airi\/assets\/js\/plugins\/min\/","theme_path":"https:\/\/airi.la-studioweb.com\/wp-content\/themes\/airi\/","ajax_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/admin-ajax.php","mm_mb_effect":"1","header_height":{"desktop":{"normal":"120","sticky":"120"},"tablet":{"normal":"100","sticky":"100"},"mobile":{"normal":"90","sticky":"90"}},"la_extension_available":{"swatches":true,"360":true,"content_type":true},"mobile_bar":"always"};
    /* ]]> */
</script>
<script src='{{URL::asset('assets/wp-content/themes/airi/assets/js/min/app.js')}}' defer ></script>
<script src='{{URL::asset('assets/wp-includes/js/wp-embed.min5010.js')}}' defer ></script>
<script src='{{URL::asset('assets/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min7e15.js')}}' defer ></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yikes_mailchimp_ajax = {"ajax_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/admin-ajax.php","page_data":{},"interest_group_checkbox_error":"This field is required.","preloader_url":"https:\/\/airi.la-studioweb.com\/wp-content\/plugins\/yikes-inc-easy-mailchimp-extender\/includes\/images\/ripple.svg","loading_dots":"https:\/\/airi.la-studioweb.com\/wp-content\/plugins\/yikes-inc-easy-mailchimp-extender\/includes\/images\/bars.svg","ajax_security_nonce":"7abc01438e"};
    var yikes_mailchimp_ajax = {"ajax_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/admin-ajax.php","page_data":{},"interest_group_checkbox_error":"This field is required.","preloader_url":"https:\/\/airi.la-studioweb.com\/wp-content\/plugins\/yikes-inc-easy-mailchimp-extender\/includes\/images\/ripple.svg","loading_dots":"https:\/\/airi.la-studioweb.com\/wp-content\/plugins\/yikes-inc-easy-mailchimp-extender\/includes\/images\/bars.svg","ajax_security_nonce":"7abc01438e"};
    /* ]]> */
</script>
<script src='{{URL::asset('assets/wp-content/plugins/yikes-inc-easy-mailchimp-extender/public/js/yikes-mc-ajax-forms.min07d0.js')}}' defer ></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var form_submission_helpers = {"ajax_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/admin-ajax.php","preloader_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/images\/wpspin_light.gif","countries_with_zip":{"US":"US","GB":"GB","CA":"CA","IE":"IE","CN":"CN","IN":"IN","AU":"AU","BR":"BR","MX":"MX","IT":"IT","NZ":"NZ","JP":"JP","FR":"FR","GR":"GR","DE":"DE","NL":"NL","PT":"PT","ES":"ES"},"page_data":{}};
    var form_submission_helpers = {"ajax_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/admin-ajax.php","preloader_url":"https:\/\/airi.la-studioweb.com\/wp-admin\/images\/wpspin_light.gif","countries_with_zip":{"US":"US","GB":"GB","CA":"CA","IE":"IE","CN":"CN","IN":"IN","AU":"AU","BR":"BR","MX":"MX","IT":"IT","NZ":"NZ","JP":"JP","FR":"FR","GR":"GR","DE":"DE","NL":"NL","PT":"PT","ES":"ES"},"page_data":{}};
    /* ]]> */
</script>
<script src='{{URL::asset('assets/wp-content/plugins/yikes-inc-easy-mailchimp-extender/public/js/form-submission-helpers.min07d0.js')}}' defer ></script>
<!-- WooCommerce JavaScript -->
<script type="text/javascript">
    jQuery(function($) {
        jQuery( 'div.woocommerce' ).on( 'click', 'a.remove', function() {
            var productID = jQuery( this ).data( 'product_id' );
            var quantity = jQuery( this ).parent().parent().find( '.qty' ).val()
            var productDetails = {
                'id': productID,
                'quantity': quantity ? quantity : '1',
            };
            _wca.push( {
                '_en': 'woocommerceanalytics_remove_from_cart',
                'blog_id': '152317838',
                'pi': productDetails.id,
                'pq': productDetails.quantity,
                'ui': 'null',
            } );
        } );
    });
</script>
</body>
</html>