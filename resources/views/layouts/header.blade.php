<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fegor Shoes</title>

    <!-- Fonts -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fegor - For the Smart and Intelligent.</title>

    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel="stylesheet" href="{{URL::asset('assets/test.css')}}"/>
    <link rel='stylesheet'
          href="{{URL::asset('assets/wp-content/plugins/revslider/public/assets/css/settings23da.css')}}"/>
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {
        }
    </style>
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <link rel='stylesheet' id='dashicons-css' href='{{URL::asset('assets/wp-includes/css/dashicons.min5010.css')}}'/>
    <link rel='stylesheet' id='js_composer_front-css'
          href='{{URL::asset('assets/wp-content/plugins/js_composer/assets/css/js_composer.min7e15.css')}}'
          media='all'/>
    <link rel='stylesheet' id='font-awesome-css'
          href='{{URL::asset('assets/wp-content/themes/airi/assets/css/font-awesome.min.css')}}' media='all'/>
    <style id='font-awesome-inline-css' type='text/css'>
        @font-face {
            font-family: 'FontAwesome';
            src: url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/fontawesome-webfont.eot')}}');
            src: url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/fontawesome-webfont.eot')}}') format('embedded-opentype'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/fontawesome-webfont.woff2')}}') format('woff2'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/fontawesome-webfont.woff')}}') format('woff'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/fontawesome-webfont.ttf')}}') format('truetype'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/fontawesome-webfont.svg')}}') format('svg');
            font-weight: normal;
            font-style: normal
        }
    </style>
    <link rel='stylesheet' id='airi-theme-css' href='{{URL::asset('assets/wp-content/themes/airi/style.css')}}'/>
    <style id='airi-theme-inline-css' type='text/css'>
        .site-loading .la-image-loading {
            opacity: 1;
            visibility: visible
        }

        .la-image-loading.spinner-custom .content {
            width: 100px;
            margin-top: -50px;
            height: 100px;
            margin-left: -50px;
            text-align: center
        }

        .la-image-loading.spinner-custom .content img {
            width: auto;
            margin: 0 auto
        }

        .site-loading #page.site {
            opacity: 0;
            transition: all .3s ease-in-out
        }

        #page.site {
            opacity: 1
        }

        .la-image-loading {
            opacity: 0;
            position: fixed;
            z-index: 999999;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            background: #fff;
            overflow: hidden;
            transition: all .3s ease-in-out;
            -webkit-transition: all .3s ease-in-out;
            visibility: hidden
        }

        .la-image-loading .content {
            position: absolute;
            width: 50px;
            height: 50px;
            top: 50%;
            left: 50%;
            margin-left: -25px;
            margin-top: -25px
        }

        .la-loader.spinner1 {
            width: 40px;
            height: 40px;
            margin: 5px;
            display: block;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            -webkit-animation: la-rotateplane 1.2s infinite ease-in-out;
            animation: la-rotateplane 1.2s infinite ease-in-out;
            border-radius: 3px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px
        }

        .la-loader.spinner2 {
            width: 40px;
            height: 40px;
            margin: 5px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            border-radius: 100%;
            -webkit-animation: la-scaleout 1.0s infinite ease-in-out;
            animation: la-scaleout 1.0s infinite ease-in-out
        }

        .la-loader.spinner3 {
            margin: 15px 0 0 -10px;
            width: 70px;
            text-align: center
        }

        .la-loader.spinner3 [class*="bounce"] {
            width: 18px;
            height: 18px;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            border-radius: 100%;
            display: inline-block;
            -webkit-animation: la-bouncedelay 1.4s infinite ease-in-out;
            animation: la-bouncedelay 1.4s infinite ease-in-out;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both
        }

        .la-loader.spinner3 .bounce1 {
            -webkit-animation-delay: -.32s;
            animation-delay: -.32s
        }

        .la-loader.spinner3 .bounce2 {
            -webkit-animation-delay: -.16s;
            animation-delay: -.16s
        }

        .la-loader.spinner4 {
            margin: 5px;
            width: 40px;
            height: 40px;
            text-align: center;
            -webkit-animation: la-rotate 2.0s infinite linear;
            animation: la-rotate 2.0s infinite linear
        }

        .la-loader.spinner4 [class*="dot"] {
            width: 60%;
            height: 60%;
            display: inline-block;
            position: absolute;
            top: 0;
            border-radius: 100%;
            -webkit-animation: la-bounce 2.0s infinite ease-in-out;
            animation: la-bounce 2.0s infinite ease-in-out;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15)
        }

        .la-loader.spinner4 .dot2 {
            top: auto;
            bottom: 0;
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s
        }

        .la-loader.spinner5 {
            margin: 5px;
            width: 40px;
            height: 40px
        }

        .la-loader.spinner5 div {
            width: 33%;
            height: 33%;
            float: left;
            -webkit-animation: la-cubeGridScaleDelay 1.3s infinite ease-in-out;
            animation: la-cubeGridScaleDelay 1.3s infinite ease-in-out
        }

        .la-loader.spinner5 div:nth-child(1), .la-loader.spinner5 div:nth-child(5), .la-loader.spinner5 div:nth-child(9) {
            -webkit-animation-delay: .2s;
            animation-delay: .2s
        }

        .la-loader.spinner5 div:nth-child(2), .la-loader.spinner5 div:nth-child(6) {
            -webkit-animation-delay: .3s;
            animation-delay: .3s
        }

        .la-loader.spinner5 div:nth-child(3) {
            -webkit-animation-delay: .4s;
            animation-delay: .4s
        }

        .la-loader.spinner5 div:nth-child(4), .la-loader.spinner5 div:nth-child(8) {
            -webkit-animation-delay: .1s;
            animation-delay: .1s
        }

        .la-loader.spinner5 div:nth-child(7) {
            -webkit-animation-delay: 0s;
            animation-delay: 0s
        }

        @-webkit-keyframes la-rotateplane {
            0% {
                -webkit-transform: perspective(120px)
            }
            50% {
                -webkit-transform: perspective(120px) rotateY(180deg)
            }
            100% {
                -webkit-transform: perspective(120px) rotateY(180deg) rotateX(180deg)
            }
        }

        @keyframes la-rotateplane {
            0% {
                transform: perspective(120px) rotateX(0deg) rotateY(0deg)
            }
            50% {
                transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg)
            }
            100% {
                transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg)
            }
        }

        @-webkit-keyframes la-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }

        @keyframes la-scaleout {
            0% {
                transform: scale(0);
                -webkit-transform: scale(0)
            }
            100% {
                transform: scale(1);
                -webkit-transform: scale(1);
                opacity: 0
            }
        }

        @-webkit-keyframes la-bouncedelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0)
            }
            40% {
                -webkit-transform: scale(1)
            }
        }

        @keyframes la-bouncedelay {
            0%, 80%, 100% {
                transform: scale(0)
            }
            40% {
                transform: scale(1)
            }
        }

        @-webkit-keyframes la-rotate {
            100% {
                -webkit-transform: rotate(360deg)
            }
        }

        @keyframes la-rotate {
            100% {
                transform: rotate(360deg);
                -webkit-transform: rotate(360deg)
            }
        }

        @-webkit-keyframes la-bounce {
            0%, 100% {
                -webkit-transform: scale(0)
            }
            50% {
                -webkit-transform: scale(1)
            }
        }

        @keyframes la-bounce {
            0%, 100% {
                transform: scale(0)
            }
            50% {
                transform: scale(1)
            }
        }

        @-webkit-keyframes la-cubeGridScaleDelay {
            0% {
                -webkit-transform: scale3d(1, 1, 1)
            }
            35% {
                -webkit-transform: scale3d(0, 0, 1)
            }
            70% {
                -webkit-transform: scale3d(1, 1, 1)
            }
            100% {
                -webkit-transform: scale3d(1, 1, 1)
            }
        }

        @keyframes la-cubeGridScaleDelay {
            0% {
                transform: scale3d(1, 1, 1)
            }
            35% {
                transform: scale3d(0, 0, 1)
            }
            70% {
                transform: scale3d(1, 1, 1)
            }
            100% {
                transform: scale3d(1, 1, 1)
            }
        }

        .la-loader.spinner1, .la-loader.spinner2, .la-loader.spinner3 [class*="bounce"], .la-loader.spinner4 [class*="dot"], .la-loader.spinner5 div {
            background-color: #cf987e
        }

        .show-when-logged {
            display: none !important
        }

        .section-page-header {
            color: #282828;
            background-repeat: repeat;
            background-position: left top;
            background-color: #f8f8f8
        }

        .section-page-header .page-title {
            color: #282828
        }

        .section-page-header a {
            color: #282828
        }

        .section-page-header a:hover {
            color: #cf987e
        }

        .section-page-header .page-header-inner {
            padding-top: 25px;
            padding-bottom: 25px
        }

        @media (min-width: 768px) {
            .section-page-header .page-header-inner {
                padding-top: 25px;
                padding-bottom: 25px
            }
        }

        @media (min-width: 992px) {
            .section-page-header .page-header-inner {
                padding-top: 40px;
                padding-bottom: 40px
            }
        }

        @media (min-width: 1440px) {
            .section-page-header .page-header-inner {
                padding-top: 70px;
                padding-bottom: 70px
            }
        }

        .site-main {
            padding-top: 0px;
            padding-bottom: 0px
        }

        body.airi-body {
            font-size: 14px;
            background-repeat: repeat;
            background-position: left top
        }

        body.airi-body.body-boxed #page.site {
            width: 1230px;
            max-width: 100%;
            margin-left: auto;
            margin-right: auto;
            background-repeat: repeat;
            background-position: left top
        }

        body.airi-body.body-boxed .site-header .site-header-inner {
            max-width: 1230px
        }

        body.airi-body.body-boxed .site-header.is-sticky .site-header-inner {
            left: calc((100% - 1230px) / 2);
            left: -webkit-calc((100% - 1230px) / 2)
        }

        #masthead_aside, .site-header .site-header-inner {
            background-repeat: repeat;
            background-position: left top;
            background-color: #fff
        }

        .enable-header-transparency .site-header:not(.is-sticky) .site-header-inner {
            background-repeat: repeat;
            background-position: left top;
            background-color: rgba(0, 0, 0, 0)
        }

        .footer-top {
            background-repeat: repeat;
            background-position: left top;
            background-attachment: scroll;
            background-color: #000000;
            padding-top: 55px;
            padding-bottom: 0
        }

        .open-newsletter-popup .lightcase-inlineWrap {
            background-image: url({{URL::asset('assets/images/shoe2.jpg')}});
            background-repeat: no-repeat;
            background-position: right center;
            background-color: #ffffff;
        }

        .header-v6 #masthead_aside .mega-menu > li > a, .header-v7 #header_aside .mega-menu > li > a, .site-main-nav .main-menu > li > a {
            font-size: 16px
        }

        .mega-menu .popup {
            font-size: 12px
        }

        .mega-menu .mm-popup-wide .inner > ul.sub-menu > li li > a, .mega-menu .mm-popup-narrow li.menu-item > a {
            font-size: 12px
        }

        .mega-menu .mm-popup-wide .inner > ul.sub-menu > li > a {
            font-size: 13px
        }

        .site-header .site-branding a {
            height: 120px;
            line-height: 120px
        }

        .site-header .header-component-inner {
            padding-top: 40px;
            padding-bottom: 40px
        }

        .site-header .header-main .la_com_action--dropdownmenu .menu, .site-header .mega-menu > li > .popup {
            margin-top: 60px
        }

        .site-header .header-main .la_com_action--dropdownmenu:hover .menu, .site-header .mega-menu > li:hover > .popup {
            margin-top: 40px
        }

        .site-header.is-sticky .site-branding a {
            height: 120px;
            line-height: 120px
        }

        .site-header.is-sticky .header-component-inner {
            padding-top: 40px;
            padding-bottom: 40px
        }

        .site-header.is-sticky .header-main .la_com_action--dropdownmenu .menu, .site-header.is-sticky .mega-menu > li > .popup {
            margin-top: 60px
        }

        .site-header.is-sticky .header-main .la_com_action--dropdownmenu:hover .menu, .site-header.is-sticky .mega-menu > li:hover > .popup {
            margin-top: 40px
        }

        @media (max-width: 1300px) and (min-width: 992px) {
            .site-header .site-branding a {
                height: 100px;
                line-height: 100px
            }

            .site-header .header-component-inner {
                padding-top: 30px;
                padding-bottom: 30px
            }

            .site-header .header-main .la_com_action--dropdownmenu .menu, .site-header .mega-menu > li > .popup {
                margin-top: 50px
            }

            .site-header .header-main .la_com_action--dropdownmenu:hover .menu, .site-header .mega-menu > li:hover > .popup {
                margin-top: 30px
            }

            .site-header.is-sticky .site-branding a {
                height: 100px;
                line-height: 100px
            }

            .site-header.is-sticky .header-component-inner {
                padding-top: 30px;
                padding-bottom: 30px
            }

            .site-header.is-sticky .header-main .la_com_action--dropdownmenu .menu, .site-header.is-sticky .mega-menu > li > .popup {
                margin-top: 50px
            }

            .site-header.is-sticky .header-main .la_com_action--dropdownmenu:hover .menu, .site-header.is-sticky .mega-menu > li:hover > .popup {
                margin-top: 30px
            }
        }

        @media (max-width: 991px) {
            .site-header-mobile .site-branding a {
                height: 90px;
                line-height: 90px
            }

            .site-header-mobile .header-component-inner {
                padding-top: 25px;
                padding-bottom: 25px
            }

            .site-header-mobile.is-sticky .site-branding a {
                height: 90px;
                line-height: 90px
            }

            .site-header-mobile.is-sticky .header-component-inner {
                padding-top: 25px;
                padding-bottom: 25px
            }
        }

        .header-v5 #masthead_aside {
            background-repeat: repeat;
            background-position: left top;
            background-color: #fff
        }

        .header-v5.enable-header-transparency #masthead_aside {
            background-repeat: repeat;
            background-position: left top;
            background-color: rgba(0, 0, 0, 0)
        }

        .widget_recent_entries .pr-item .pr-item--right a, .single_post_quote_wrap .quote-wrapper .format-content, .la_testimonials--style-2 .loop__item__desc, .la_testimonials--style-3 .loop__item__desc, .la_testimonials--style-4 .loop__item__desc, .la_testimonials--style-5 .loop__item__desc, .la_testimonials--style-7 .loop__item__desc, .la_testimonials--style-9 .loop__item__desc, .la-sc-icon-boxes.icon-type-number .type-number, .member--style-1 .loop__item__meta, .member--style-2 .member__item__role, .member--style-3 .member__item__role, .banner-type-5 .b-title1, .la-blockquote.style-2 p, .la-blockquote.style-3 p, .la_testimonials--style-1 .loop__item__desc, .la_testimonials--style-1 .testimonial_item--role, .la_testimonials--style-8 .loop__item__desc, .elm-countdown.elm-countdown-style-3 .countdown-period, .elm-countdown.elm-countdown-style-4 .countdown-period, .la-blockquote.style-4 p, .three-font-family, .highlight-font-family {
            font-family: "Playfair Display", "Helvetica Neue", Arial, sans-serif;
        }

        h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6, .la-service-box.service-type-3 .b-title1, .heading-font-family {
            font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif;
        }

        body, .la-blockquote.style-1 footer {
            font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif;
        }

        .background-color-primary, .slick__nav_style1 .slick-slider .slick-arrow:hover, .item--link-overlay:before, .dl-menu .tip.hot, .mega-menu .tip.hot, .menu .tip.hot, .comment-form .form-submit input:hover, .la_testimonials--style-4 .loop__item__desc:after, .pf-default.pf-style-1 .loop__item__info:after, .pf-default.pf-style-2 .loop__item__info, .pf-default.pf-style-4 .loop__item__info:after, .pf-default.pf-style-5 .loop__item__thumbnail--linkoverlay:before, .member--style-4 .loop__item__thumbnail .item--social a:hover, .member--style-7 .loop__item__thumbnail .item--social a:hover, .pricing.style-1:hover .pricing__action a, .pricing.style-4:hover .pricing__action a, .pricing.style-5:hover .pricing__action a, .banner-type-10 .banner--link-overlay:hover .hidden, .woocommerce > .return-to-shop .button:hover, .la-newsletter-popup .yikes-easy-mc-form .yikes-easy-mc-submit-button:hover, .la_hotspot_sc[data-style="color_pulse"] .la_hotspot, .la_hotspot_sc .la_hotspot_wrap .nttip, .single-release-content .lastudio-release-buttons .lastudio-release-button a, .social-media-link.style-round a:hover, .social-media-link.style-square a:hover, .social-media-link.style-circle a:hover, .social-media-link.style-outline a:hover, .social-media-link.style-circle-outline a:hover, .la-timeline-wrap.style-1 .timeline-block .timeline-dot, .products-list .product_item .product_item--thumbnail .product_item--action .quickview:hover, .products-grid-3 .product_item_thumbnail_action .button:hover, .products-grid-2 .product_item_thumbnail_action .button:hover, .products-grid-1 .product_item_thumbnail_action .button:hover, .woocommerce.special_offers .product_item--info .la-custom-badge, .la-woo-product-gallery > .woocommerce-product-gallery__trigger, .product--summary .single_add_to_cart_button:hover, .wc_tabs_at_bottom .wc-tabs li.active > a:after, .custom-product-wrap .block_heading--title span:after, .woocommerce-MyAccount-navigation li:hover a, .woocommerce-MyAccount-navigation li.is-active a, .registration-form .button, .socials-color a:hover {
            background-color: #cf987e;
        }

        .background-color-secondary, .la-pagination ul .page-numbers.current, .la-pagination ul .page-numbers:hover, .slick-slider .slick-dots button, .wc-toolbar .wc-ordering ul li:hover a, .wc-toolbar .wc-ordering ul li.active a, .widget_layered_nav.widget_layered_nav--borderstyle li:hover a, .widget_layered_nav.widget_layered_nav--borderstyle li.active a, .showposts-loop.showposts-list .btn-readmore:hover, .showposts-loop.grid-3 .btn-readmore:hover, .showposts-loop.grid-4 .btn-readmore:hover, .comment-form .form-submit input, .pf-default.pf-style-3 .loop__item__info:after, .pricing.style-1 .pricing__action a, .woocommerce > .return-to-shop .button, .la-newsletter-popup .yikes-easy-mc-form .yikes-easy-mc-submit-button, .single-release-content .lastudio-release-buttons .lastudio-release-button a:hover, .social-media-link.style-round a, .social-media-link.style-square a, .social-media-link.style-circle a, .product--summary .single_add_to_cart_button {
            background-color: #282828;
        }

        .background-color-secondary, .socials-color a {
            background-color: #282828;
        }

        .background-color-body {
            background-color: #8a8a8a;
        }

        .background-color-border {
            background-color: #a3a3a3;
        }

        a:hover, .elm-loadmore-ajax a:hover, .search-form .search-button:hover, .slick-slider .slick-dots li:hover span, .slick-slider .slick-dots .slick-active span, .slick-slider .slick-arrow:hover, .la-slick-nav .slick-arrow:hover, .vertical-style ul li:hover a, .vertical-style ul li.active a, .widget.widget_product_tag_cloud a.active, .widget.widget_product_tag_cloud .active a, .widget.product-sort-by .active a, .widget.widget_layered_nav .active a, .widget.la-price-filter-list .active a, .product_list_widget a:hover, #header_aside .btn-aside-toggle:hover, .dl-menu .tip.hot .tip-arrow:before, .mega-menu .tip.hot .tip-arrow:before, .menu .tip.hot .tip-arrow:before, .showposts-loop.showposts-list.list-2 .loop__item__meta__top, .showposts-loop.grid-5 .loop__item__meta__top, .la_testimonials--style-2 .entry-title, .la_testimonials--style-3 .entry-title, ul.list-icon-checked li:before, ol.list-icon-checked li:before, .list-icon-checked .wpb_wrapper > ol li:before, .list-icon-checked .wpb_wrapper > ul li:before, ul.list-icon-checked2 li:before, ol.list-icon-checked2 li:before, .list-icon-checked2 .wpb_wrapper > ol li:before, .list-icon-checked2 .wpb_wrapper > ul li:before, ul.list-icon-dots li:before, ol.list-icon-dots li:before, .list-icon-dots .wpb_wrapper > ol li:before, .list-icon-dots .wpb_wrapper > ul li:before, .vc_custom_heading.heading__button2 a:hover, .member--style-5 .entry-title, .member--style-7 .loop__item__meta, .wpb-js-composer .la__tta .vc_active .vc_tta-panel-heading .vc_tta-panel-title, .la__ttaac > .vc_tta.accordion--1 .vc_tta-panel.vc_active .vc_tta-title-text, .la-service-box.service-type-4 .box-inner:hover .b-title1, .easy_mc__style1 .yikes-easy-mc-form .yikes-easy-mc-submit-button:hover, .easy_mc__style3 .yikes-easy-mc-form .yikes-easy-mc-submit-button:hover, .la-lists-icon .la-sc-icon-item > span, ul.list-dots.primary > li:before, ul.list-checked.primary > li:before, body .vc_toggle.vc_toggle_default.vc_toggle_active .vc_toggle_title h4, .la-timeline-wrap.style-1 .timeline-block .timeline-subtitle, .product_item--thumbnail .elm-countdown .countdown-amount, .product_item .price ins, .product--summary .social--sharing a:hover, .product--summary .add_compare:hover, .product--summary .add_wishlist:hover, .cart-collaterals .woocommerce-shipping-calculator .button:hover, .cart-collaterals .la-coupon .button:hover, #customer_login .woocommerce-privacy-policy-text a, p.lost_password {
            color: #cf987e;
        }

        .text-color-primary {
            color: #cf987e !important;
        }

        .swatch-wrapper:hover, .swatch-wrapper.selected, .member--style-2 .loop__item__thumbnail:after, .member--style-3 .loop__item__info:after, .member--style-3 .loop__item__info:before, .la__tta .tab--2 .vc_tta-tabs-list .vc_active a, .la__tta .tab--4 .vc_tta-tabs-list .vc_active .vc_tta-title-text, .banner-type-7 .box-inner:hover .banner--btn, .banner-type-7 .banner--btn:hover, .la-service-box.service-type-4 .box-inner:hover, .social-media-link.style-outline a:hover, .social-media-link.style-circle-outline a:hover {
            border-color: #cf987e;
        }

        .border-color-primary {
            border-color: #cf987e !important;
        }

        .border-top-color-primary {
            border-top-color: #cf987e !important;
        }

        .border-bottom-color-primary {
            border-bottom-color: #cf987e !important;
        }

        .border-left-color-primary {
            border-left-color: #cf987e !important;
        }

        .border-right-color-primary {
            border-right-color: #cf987e !important;
        }

        .woocommerce-message, .woocommerce-error, .woocommerce-info, .form-row label, .wc-toolbar .woocommerce-result-count, .wc-toolbar .wc-view-toggle .active, .wc-toolbar .wc-view-count li.active, div.quantity, .widget_recent_entries .pr-item .pr-item--right a:not(:hover), .widget_recent_comments li.recentcomments a, .product_list_widget a, .product_list_widget .amount, .sf-fields .search-field:focus, #header_aside .btn-aside-toggle, .widget.widget_product_tag_cloud .tagcloud, .sidebar-inner .dokan-category-menu #cat-drop-stack > ul li.parent-cat-wrap, .showposts-loop .loop__item__meta--footer, .author-info__name, .author-info__link, .post-navigation .blog_pn_nav-title, .post-navigation .blog_pn_nav-text, .commentlist .comment-meta .comment-author, .woocommerce-Reviews .woocommerce-review__author, .woocommerce-Reviews .woocommerce-Reviews-title, .comments-container .comments-title h3, .comment-respond .comment-reply-title, .portfolio-nav, .pf-info-wrapper .pf-info-label, .pf-info-wrapper .social--sharing a:hover, .la_testimonials--style-1 .loop__item__desc, .la_testimonials--style-2 .testimonial_item, .la_testimonials--style-3 .loop__item__desc, .la_testimonials--style-4, .la_testimonials--style-7 .loop__item__inner, .la_testimonials--style-9 .loop__item__inner2, .la_testimonials--style-10 .loop__item__inner2, .ib-link-read_more .icon-boxes-inner > a:not(:hover), .vc_custom_heading.heading__button a, .vc_custom_heading.heading__button2 a, .vc_custom_heading.heading__button_intab a, .pf-default.pf-style-6 .loop__item__info, .pf-special.pf-style-1 .loop__item__info, .member--style-2 .member__item__role, .member--style-3 .member__item__role, .member--style-3 .item--social, .member--style-5 .loop__item__info, .pricing.style-2 .pricing__price-box, .pricing.style-3 .pricing__title, .pricing.style-3 .pricing__price-box, .elm-countdown.elm-countdown-style-1 .countdown-amount, .elm-countdown.elm-countdown-style-3 .countdown-amount, .elm-countdown.elm-countdown-style-4, .la__tta .tab--1 .vc_tta-tabs-list .vc_active a, .la__tta .tab--2 .vc_tta-tabs-list li a, .la__tta .tab--3 .vc_tta-tabs-list .vc_active a, .la__tta .tab--4 .vc_tta-tabs-list .vc_active a, .la-service-box.service-type-1 .banner--info, .la-service-box.service-type-3 .b-title1, .la-service-box.service-type-4 .b-title1, .la-service-box.service-type-5, .easy_mc__style1 .yikes-easy-mc-form .yikes-easy-mc-email:focus, .easy_mc__style1 .yikes-easy-mc-form .yikes-easy-mc-submit-button, .easy_mc__style3 .yikes-easy-mc-form .yikes-easy-mc-submit-button, .single-release-content .release-info-container .release-meta-container strong, .la-blockquote.style-4, .la-blockquote.style-3, ul.list-dots.secondary > li:before, ul.list-checked.secondary > li:before, .product_item--info .elm-countdown .countdown-amount, .product_item .price > .amount, .products-list .product_item .price, .products-list .product_item .product_item--info .add_compare, .products-list .product_item .product_item--info .add_wishlist, .products-list-mini .product_item .price, .products-list .product_item .product_item--thumbnail .product_item--action .quickview, .products-grid-3 .product_item_thumbnail_action .button, .products-grid-2 .product_item_thumbnail_action .button, .products-grid-1 .product_item_thumbnail_action .button, .la-woo-thumbs .slick-arrow, .product--summary .entry-summary > .stock.in-stock, .product--summary .product-nextprev, .product--summary .single-price-wrapper .price ins .amount, .product--summary .single-price-wrapper .price > .amount, .product--summary .product_meta, .product--summary .product_meta_sku_wrapper, .product--summary .product-share-box, .product--summary .group_table td, .product--summary .variations td, .product--summary .add_compare, .product--summary .add_wishlist, .wc-tabs li:hover > a, .wc-tabs li.active > a, .wc-tab .wc-tab-title, #tab-description .tab-content, .shop_table td.product-price, .shop_table td.product-subtotal, .cart-collaterals .shop_table, .cart-collaterals .woocommerce-shipping-calculator .button, .cart-collaterals .la-coupon .button, .woocommerce > p.cart-empty, table.woocommerce-checkout-review-order-table, .wc_payment_methods .wc_payment_method label, .woocommerce-order ul strong, .blog-main-loop__btn-loadmore {
            color: #282828;
        }

        .text-color-secondary {
            color: #282828 !important;
        }

        input:focus, select:focus, textarea:focus, .showposts-loop.showposts-list .btn-readmore:hover, .showposts-loop.grid-3 .btn-readmore:hover, .showposts-loop.grid-4 .btn-readmore:hover, .vc_custom_heading.heading__button a:hover, .vc_custom_heading.heading__button_intab a:hover {
            border-color: #282828;
        }

        .border-color-secondary {
            border-color: #282828 !important;
        }

        .border-top-color-secondary {
            border-top-color: #282828 !important;
        }

        .border-bottom-color-secondary {
            border-bottom-color: #282828 !important;
        }

        .border-left-color-secondary {
            border-left-color: #282828 !important;
        }

        .border-right-color-secondary {
            border-right-color: #282828 !important;
        }

        h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6, table th, .sidebar-inner ul.menu li, .sidebar-inner .dokan-category-menu .widget-title, .product--summary .social--sharing a, .extradiv-after-frm-cart {
            color: #282828;
        }

        .text-color-heading {
            color: #282828 !important;
        }

        .border-color-heading {
            border-color: #282828 !important;
        }

        .border-top-color-heading {
            border-top-color: #282828 !important;
        }

        .border-bottom-color-heading {
            border-bottom-color: #282828 !important;
        }

        .border-left-color-heading {
            border-left-color: #282828 !important;
        }

        .border-right-color-heading {
            border-right-color: #282828 !important;
        }

        .text-color-three {
            color: #a3a3a3 !important;
        }

        .border-color-three {
            border-color: #a3a3a3 !important;
        }

        .border-top-color-three {
            border-top-color: #a3a3a3 !important;
        }

        .border-bottom-color-three {
            border-bottom-color: #a3a3a3 !important;
        }

        .border-left-color-three {
            border-left-color: #a3a3a3 !important;
        }

        .border-right-color-three {
            border-right-color: #a3a3a3 !important;
        }

        body, .la__tta .tab--3 .vc_tta-tabs-list, .easy_mc__style1 .yikes-easy-mc-form .yikes-easy-mc-email, table.woocommerce-checkout-review-order-table .variation, table.woocommerce-checkout-review-order-table .product-quantity {
            color: #8a8a8a;
        }

        .text-color-body {
            color: #8a8a8a !important;
        }

        .border-color-body {
            border-color: #8a8a8a !important;
        }

        .border-top-color-body {
            border-top-color: #8a8a8a !important;
        }

        .border-bottom-color-body {
            border-bottom-color: #8a8a8a !important;
        }

        .border-left-color-body {
            border-left-color: #8a8a8a !important;
        }

        .border-right-color-body {
            border-right-color: #8a8a8a !important;
        }

        input, select, textarea, table, table th, table td, .share-links a, .select2-container .select2-selection--single, .swatch-wrapper, .widget_shopping_cart_content .total, .calendar_wrap caption, .widget-border.widget, .widget-border-bottom.widget, .easy_mc__style1 .yikes-easy-mc-form .yikes-easy-mc-email, .social-media-link.style-outline a, body .vc_toggle.vc_toggle_default, .la-timeline-wrap.style-1 .timeline-line, .la-timeline-wrap.style-2 .timeline-title:after, .shop_table.woocommerce-cart-form__contents td, .showposts-loop.main-search-loop .btn-readmore {
            border-color: #a3a3a3;
        }

        .border-color {
            border-color: #a3a3a3 !important;
        }

        .border-top-color {
            border-top-color: #a3a3a3 !important;
        }

        .border-bottom-color {
            border-bottom-color: #a3a3a3 !important;
        }

        .border-left-color {
            border-left-color: #a3a3a3 !important;
        }

        .border-right-color {
            border-right-color: #a3a3a3 !important;
        }

        .btn {
            color: #fff;
            background-color: #282828;
        }

        .btn:hover {
            background-color: #cf987e;
            color: #fff;
        }

        .btn.btn-primary {
            background-color: #cf987e;
            color: #fff;
        }

        .btn.btn-primary:hover {
            color: #fff;
            background-color: #282828;
        }

        .btn.btn-outline {
            border-color: #a3a3a3;
            color: #282828;
        }

        .btn.btn-outline:hover {
            color: #fff;
            background-color: #cf987e;
            border-color: #cf987e;
        }

        .btn.btn-style-flat.btn-color-primary {
            background-color: #cf987e;
        }

        .btn.btn-style-flat.btn-color-primary:hover {
            background-color: #282828;
        }

        .btn.btn-style-flat.btn-color-white {
            background-color: #fff;
            color: #282828;
        }

        .btn.btn-style-flat.btn-color-white:hover {
            color: #fff;
            background-color: #cf987e;
        }

        .btn.btn-style-flat.btn-color-white2 {
            background-color: #fff;
            color: #282828;
        }

        .btn.btn-style-flat.btn-color-white2:hover {
            color: #fff;
            background-color: #282828;
        }

        .btn.btn-style-flat.btn-color-gray {
            background-color: #8a8a8a;
        }

        .btn.btn-style-flat.btn-color-gray:hover {
            background-color: #cf987e;
        }

        .btn.btn-style-outline:hover {
            border-color: #cf987e;
            background-color: #cf987e;
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-black {
            border-color: #282828;
            color: #282828;
        }

        .btn.btn-style-outline.btn-color-black:hover {
            border-color: #cf987e;
            background-color: #cf987e;
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-primary {
            border-color: #cf987e;
            color: #cf987e;
        }

        .btn.btn-style-outline.btn-color-primary:hover {
            border-color: #282828;
            background-color: #282828;
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-white {
            border-color: #fff;
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-white:hover {
            border-color: #cf987e;
            background-color: #cf987e;
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-white2 {
            border-color: rgba(255, 255, 255, 0.5);
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-white2:hover {
            border-color: #282828;
            background-color: #282828;
            color: #fff;
        }

        .btn.btn-style-outline.btn-color-gray {
            border-color: rgba(35, 35, 36, 0.2);
            color: #282828;
        }

        .btn.btn-style-outline.btn-color-gray:hover {
            border-color: #cf987e;
            background-color: #cf987e;
            color: #fff !important;
        }

        .woocommerce.add_to_cart_inline a {
            border-color: #a3a3a3;
            color: #282828;
        }

        .woocommerce.add_to_cart_inline a:hover {
            background-color: #282828;
            border-color: #282828;
            color: #fff;
        }

        .elm-loadmore-ajax a {
            color: #282828;
        }

        .elm-loadmore-ajax a:hover {
            color: #cf987e;
            border-color: #cf987e;
        }

        form.track_order .button, .place-order .button, .wc-proceed-to-checkout .button, .widget_shopping_cart_content .button, .woocommerce-MyAccount-content form .button, .lost_reset_password .button, form.register .button, .checkout_coupon .button, .woocomerce-form .button {
            background-color: #282828;
            border-color: #282828;
            color: #fff;
            min-width: 150px;
        }

        form.track_order .button:hover, .place-order .button:hover, .wc-proceed-to-checkout .button:hover, .widget_shopping_cart_content .button:hover, .woocommerce-MyAccount-content form .button:hover, .lost_reset_password .button:hover, form.register .button:hover, .checkout_coupon .button:hover, .woocomerce-form .button:hover {
            background-color: #cf987e;
            border-color: #cf987e;
            color: #fff;
        }

        .shop_table.cart td.actions .button {
            background-color: transparent;
            color: #282828;
            border-color: #a3a3a3;
        }

        .shop_table.cart td.actions .button:hover {
            color: #fff;
            background-color: #282828;
            border-color: #282828;
        }

        .widget_price_filter .button {
            color: #fff;
            background-color: #282828;
        }

        .widget_price_filter .button:hover {
            color: #fff;
            background-color: #cf987e;
        }

        #header_menu_burger, #masthead_aside, #header_aside {
            background-color: #fff;
            color: #282828;
        }

        #header_menu_burger h1, #header_menu_burger .h1, #header_menu_burger h2, #header_menu_burger .h2, #header_menu_burger h3, #header_menu_burger .h3, #header_menu_burger h4, #header_menu_burger .h4, #header_menu_burger h5, #header_menu_burger .h5, #header_menu_burger h6, #header_menu_burger .h6, #masthead_aside h1, #masthead_aside .h1, #masthead_aside h2, #masthead_aside .h2, #masthead_aside h3, #masthead_aside .h3, #masthead_aside h4, #masthead_aside .h4, #masthead_aside h5, #masthead_aside .h5, #masthead_aside h6, #masthead_aside .h6, #header_aside h1, #header_aside .h1, #header_aside h2, #header_aside .h2, #header_aside h3, #header_aside .h3, #header_aside h4, #header_aside .h4, #header_aside h5, #header_aside .h5, #header_aside h6, #header_aside .h6 {
            color: #282828;
        }

        #header_menu_burger ul:not(.sub-menu) > li > a, #masthead_aside ul:not(.sub-menu) > li > a, #header_aside ul:not(.sub-menu) > li > a {
            color: #282828;
        }

        #header_menu_burger ul:not(.sub-menu) > li:hover > a, #masthead_aside ul:not(.sub-menu) > li:hover > a, #header_aside ul:not(.sub-menu) > li:hover > a {
            color: #cf987e;
        }

        .header--aside .header_component--dropdown-menu .menu {
            background-color: #fff;
        }

        .header--aside .header_component > a {
            color: #282828;
        }

        .header--aside .header_component:hover > a {
            color: #cf987e;
        }

        ul.mega-menu .popup li > a {
            color: #8A8A8A;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .popup li:hover > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .popup li.active > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .mm-popup-wide .popup li.mm-item-level-2 > a {
            color: #8A8A8A;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .mm-popup-wide .popup li.mm-item-level-2:hover > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .mm-popup-wide .popup li.mm-item-level-2.active > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .popup > .inner, ul.mega-menu .mm-popup-wide .inner > ul.sub-menu > li li ul.sub-menu, ul.mega-menu .mm-popup-narrow ul ul {
            background-color: #fff;
        }

        ul.mega-menu .mm-popup-wide .inner > ul.sub-menu > li li li:hover > a, ul.mega-menu .mm-popup-narrow li.menu-item:hover > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .mm-popup-wide .inner > ul.sub-menu > li li li.active > a, ul.mega-menu .mm-popup-narrow li.menu-item.active > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        ul.mega-menu .mm-popup-wide .popup > .inner {
            background-color: #fff;
        }

        ul.mega-menu .mm-popup-wide .inner > ul.sub-menu > li > a {
            color: #282828;
        }

        .site-main-nav .main-menu > li > a {
            color: #282828;
            background-color: rgba(0, 0, 0, 0);
        }

        .site-main-nav .main-menu > li.active > a, .site-main-nav .main-menu > li:hover > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        .site-header .header_component > .component-target {
            color: #282828;
        }

        .site-header .header_component--linktext:hover > a .component-target-text, .site-header .header_component--linktext:hover > a > i, .site-header .header_component:not(.la_com_action--linktext):hover > a {
            color: #cf987e;
        }

        .enable-header-transparency .site-header:not(.is-sticky) .header_component > .component-target {
            color: #fff;
        }

        .enable-header-transparency .site-header:not(.is-sticky) .header_component > a {
            color: #fff;
        }

        .enable-header-transparency .site-header:not(.is-sticky) .header_component:hover > a {
            color: #cf987e;
        }

        .enable-header-transparency .site-header:not(.is-sticky) .site-main-nav .main-menu > li > a {
            color: #fff;
            background-color: rgba(0, 0, 0, 0);
        }

        .enable-header-transparency .site-header:not(.is-sticky) .site-main-nav .main-menu > li.active > a, .enable-header-transparency .site-header:not(.is-sticky) .site-main-nav .main-menu > li:hover > a {
            color: #ffffff;
            background-color: rgba(0, 0, 0, 0);
        }

        .enable-header-transparency .site-header:not(.is-sticky) .site-main-nav .main-menu > li.active:before, .enable-header-transparency .site-header:not(.is-sticky) .site-main-nav .main-menu > li:hover:before {
            background-color: rgba(0, 0, 0, 0);
        }

        .site-header-mobile .site-header-inner {
            background-color: #fff;
        }

        .site-header-mobile .header_component > .component-target {
            color: #8A8A8A;
        }

        .site-header-mobile .mobile-menu-wrap {
            background-color: #fff;
        }

        .site-header-mobile .mobile-menu-wrap .dl-menuwrapper ul {
            background: #fff;
            border-color: rgba(140, 140, 140, 0.2);
        }

        .site-header-mobile .mobile-menu-wrap .dl-menuwrapper li {
            border-color: rgba(140, 140, 140, 0.2);
        }

        .site-header-mobile .mobile-menu-wrap .dl-menu > li > a {
            color: #282828;
            background-color: rgba(0, 0, 0, 0);
        }

        .site-header-mobile .mobile-menu-wrap .dl-menu > li:hover > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        .site-header-mobile .mobile-menu-wrap .dl-menu ul > li > a {
            color: #282828;
            background-color: rgba(0, 0, 0, 0);
        }

        .site-header-mobile .mobile-menu-wrap .dl-menu ul > li:hover > a {
            color: #fff;
            background-color: #cf987e;
        }

        .enable-header-transparency .site-header-mobile:not(.is-sticky) .site-header-inner {
            background-color: #fff;
        }

        .enable-header-transparency .site-header-mobile:not(.is-sticky) .header_component > .component-target {
            color: #8a8a8a;
        }

        .site-header .site-header-top {
            background-color: rgba(0, 0, 0, 0);
            color: #8a8a8a;
        }

        .site-header .site-header-top .header_component .component-target {
            color: #8a8a8a;
        }

        .site-header .site-header-top .header_component a.component-target {
            color: #8a8a8a;
        }

        .site-header .site-header-top .header_component:hover a .component-target-text {
            color: #cf987e;
        }

        .enable-header-transparency .site-header .site-header-top {
            background-color: rgba(0, 0, 0, 0);
            color: #ffffff;
        }

        .enable-header-transparency .site-header .site-header-top .header_component .component-target {
            color: #ffffff;
        }

        .enable-header-transparency .site-header .site-header-top .header_component a.component-target {
            color: #fff;
        }

        .enable-header-transparency .site-header .site-header-top .header_component:hover a .component-target-text {
            color: #cf987e;
        }

        .cart-flyout {
            background-color: #fff;
            color: #282828;
        }

        .cart-flyout .cart-flyout__heading {
            color: #282828;
            font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif;
        }

        .cart-flyout .product_list_widget a {
            color: #282828;
        }

        .cart-flyout .product_list_widget a:hover {
            color: #cf987e;
        }

        .cart-flyout .widget_shopping_cart_content .total {
            color: #282828;
        }

        .footer-top {
            color: #868686;
        }

        .footer-top a {
            color: #868686;
        }

        .footer-top a:hover {
            color: #cf987e;
        }

        .footer-top .widget .widget-title {
            color: #fff;
        }

        .footer-bottom {
            background-color: #000000;
            color: #868686;
        }

        .footer-bottom a {
            color: #868686;
        }

        .footer-bottom a:hover {
            color: #cf987e;
        }

        .site-header-mobile .mobile-menu-wrap .dl-menu {
            border-width: 1px 0 0;
            border-style: solid;
            box-shadow: 0 6px 12px rgba(0, 0, 0, 0.076);
            -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.076);
        }

        .site-header-mobile .mobile-menu-wrap .dl-menu li {
            border-width: 1px 0 0;
            border-style: solid;
        }

        .site-header-mobile .mobile-menu-wrap .dl-menuwrapper li.dl-subviewopen, .site-header-mobile .mobile-menu-wrap .dl-menuwrapper li.dl-subview, .site-header-mobile .mobile-menu-wrap .dl-menuwrapper li:first-child {
            border-top-width: 0;
        }

        .wpb-js-composer [class*="vc_tta-la-"] .vc_tta-panel-heading .vc_tta-panel-title .vc_tta-icon {
            margin-right: 10px;
        }

        .la-myaccount-page .la_tab_control li.active a, .la-myaccount-page .la_tab_control li:hover a, .la-myaccount-page .ywsl-label {
            color: #282828;
        }

        .la-myaccount-page .btn-create-account:hover {
            color: #fff;
            background-color: #282828;
            border-color: #282828;
        }

        .btn.btn-style-outline-bottom:hover {
            background: none !important;
            color: #cf987e !important;
            border-color: #cf987e !important;
        }

        @media (max-width: 767px) {
            .la-advanced-product-filters {
                background-color: #fff;
                color: #282828;
            }

            .la-advanced-product-filters .widget-title {
                color: #282828;
            }

            .la-advanced-product-filters a {
                color: #282828;
            }

            .la-advanced-product-filters a:hover {
                color: #cf987e;
            }
        }

        .nav-menu-burger {
            color: #282828;
        }

        .header-v7 #header_aside, .header-v6 #masthead_aside {
            color: #282828;
        }

        .header-v7 #header_aside .header_component > a, .header-v6 #masthead_aside .header_component > a {
            color: #282828;
        }

        .header-v7 #header_aside .header_component:hover > a, .header-v6 #masthead_aside .header_component:hover > a {
            color: #cf987e;
        }

        .header-v7 #header_aside .main-menu > li > a, .header-v6 #masthead_aside .main-menu > li > a {
            color: #282828;
            background-color: rgba(0, 0, 0, 0);
        }

        .header-v7 #header_aside .main-menu > li:hover > a, .header-v7 #header_aside .main-menu > li.open > a, .header-v7 #header_aside .main-menu > li.active > a, .header-v6 #masthead_aside .main-menu > li:hover > a, .header-v6 #masthead_aside .main-menu > li.open > a, .header-v6 #masthead_aside .main-menu > li.active > a {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        .header-v7.enable-header-transparency #header_aside, .header-v6.enable-header-transparency #masthead_aside {
            color: #fff;
        }

        .header-v7.enable-header-transparency #header_aside .header_component > a, .header-v6.enable-header-transparency #masthead_aside .header_component > a {
            color: #fff;
        }

        .header-v7.enable-header-transparency #header_aside .header_component:hover > a, .header-v6.enable-header-transparency #masthead_aside .header_component:hover > a {
            color: #cf987e;
        }

        .header-v7.enable-header-transparency #header_aside .main-menu > li > a, .header-v6.enable-header-transparency #masthead_aside .main-menu > li > a {
            color: #fff;
            background-color: rgba(0, 0, 0, 0);
        }

        .header-v7.enable-header-transparency #header_aside .main-menu > li:hover a, .header-v7.enable-header-transparency #header_aside .main-menu > li.open a, .header-v7.enable-header-transparency #header_aside .main-menu > li.active a, .header-v6.enable-header-transparency #masthead_aside .main-menu > li:hover > a, .header-v6.enable-header-transparency #masthead_aside .main-menu > li.open > a, .header-v6.enable-header-transparency #masthead_aside .main-menu > li.active > a {
            color: #ffffff;
            background-color: rgba(0, 0, 0, 0);
        }

        .header-v8 .site-header__nav-primary .site-category-nav .toggle-category-menu {
            color: #282828;
            background-color: rgba(0, 0, 0, 0);
        }

        .header-v8 .site-header__nav-primary .site-category-nav:hover .toggle-category-menu {
            color: #cf987e;
            background-color: rgba(0, 0, 0, 0);
        }

        @font-face {
            font-family: 'dl-icon';
            src: url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/dl-icon.eot')}}');
            src: url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/dl-icon.eot')}}') format('embedded-opentype'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/dl-icon.woff')}}') format('woff'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/dl-icon.ttf')}}') format('truetype'),
            url('{{URL::asset('assets/wp-content/themes/airi/assets/fonts/dl-icon.svg')}}') format('svg');
            font-weight: normal;
            font-style: normal
        }
    </style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1536941754577{background-image: url({{URL::asset('assets/wp-content/uploads/2018/09/about-bg1.jpg')}}) !important;}.vc_custom_1538017460500{padding-top: 0px !important;}.vc_custom_1536942001135{padding-top: 0px !important;}</style>
    <link rel='stylesheet' id='airi-child-style-css'
          href='{{URL::asset('assets/wp-content/themes/airi-child/style5152.css')}}'/>
    <link rel='stylesheet' id='animate-css-css'
          href='{{URL::asset('assets/wp-content/themes/airi/assets/css/animate.min.css')}}'/>
    <link rel='stylesheet' id='airi-google_fonts-css'
          href='https://fonts.googleapis.com/css?family=Montserrat:300,300italic,regular,italic,700,700italic%7CPlayfair+Display:regular,italic,700,700italic'
          media='all'/>
    <link rel="stylesheet" href="{{ URL::asset('assets/revslider.css') }}"/>
    <link rel="stylesheet" href="{{ URL::asset('assets/masterslider/style/masterslider.css') }}" />
    <link href="{{ URL::asset('assets/masterslider/skins/default/style.css') }}" rel='stylesheet' type='text/css'>
    <link href='{{ URL::asset('assets/masterslider/style/ms-fullscreen.css') }}' rel='stylesheet' type='text/css'>

    <script src='{{URL::asset('assets/wp-includes/js/jquery/jqueryb8ff.js')}}'></script>
    <script src='{{URL::asset('assets/wp-includes/js/jquery/jquery-migrate.min330a.js')}}'></script>
    <script src='{{URL::asset('assets/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min23da.js')}}'
            defer></script>
    <script src='{{URL::asset('assets/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js')}}'
            defer></script>
    <!--[if lt IE 9]>
    <script src='{{URL::asset(' assets/wp-content/themes/airi/assets/js/enqueue/min/respond.js')}}'></script>
    <
    script
    src = "{{ URL::asset('assets/revslider.js') }}" ></script>
    <![endif]-->


    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="https://airi.la-studioweb.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css"
          media="screen"><![endif]-->
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface."/>
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };</script>
    <style id="airi-extra-custom-css">
        .site-footer ul li {
            margin-bottom: 15px;
        }

        .footer-top .widget .widget-title {
            text-transform: uppercase;
            font-weight: 500;
            letter-spacing: 2px;
        }

        .footer-top .widget .widget-title:after {
            content: "";
            border-bottom: 1px solid #CF987E;
            display: block;
            width: 30px;
            padding-top: 20px;
        }

        .site-footer .la-contact-info .la-contact-item {
            margin-bottom: 15px;
        }

        .site-footer .la-contact-info .la-contact-address {
            line-height: normal;
        }

        .isLaWebRoot .la-footer-5col32223 .footer-column-5 .footer-column-inner {
            width: 100%;
            float: none;
        }

        .la-footer-5col32223 .footer-column-1 .footer-column-inner {
            width: 300px;
        }

        .footer-bottom .footer-bottom-inner .la-headings {
            position: relative;
        }

        .footer-bottom .footer-bottom-inner .la-headings:before {
            content: "";
            height: 48px;
            width: 1px;
            background: #3E3E3E;
            position: absolute;
            left: -50px;
            top: 5px;
            opacity: 0.5;
        }

        .footer-bottom .footer-bottom-inner .col-md-3:first-child .la-headings:before {
            display: none;
        }

        .footer-bottom .footer-bottom-inner {
            padding-top: 25px;
        }

        @media (min-width: 1200px) {
            .la-footer-5col32223 .footer-column {
                width: 16%;
            }

            .la-footer-5col32223 .footer-column-1 {
                width: 30%;
            }

            .la-footer-5col32223 .footer-column-5 {
                width: 22%;
            }
        }
    </style>
    <script>try {
        } catch (ex) {
        }</script>
    <style type="text/css" data-type="vc_custom-css">.image_max_width .vc_figure {
            max-width: 60%;
        }</style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1535617477760 {
            background-color: #f9f9f9 !important;
        }

        .vc_custom_1535618228226 {
            background-color: #f9f9f9 !important;
        }

        .vc_custom_1535617632712 {
            padding-top: 0px !important;
        }</style>
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
</head>

<body class="home page-template-default page page-id-12 woocommerce-no-js isLaWebRoot airi-body lastudio-airi header-v3 header-mb-v2 footer-v5col32223 body-col-1c page-title-vhide enable-header-transparency enable-header-sticky enable-header-fullwidth site-loading wpb-js-composer js-comp-ver-5.5.4 vc_responsive">

<div class="la-image-loading">
    <div class="content">
        <div class="la-loader spinner3">
            <div class="dot1"></div>
            <div class="dot2"></div>
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
            <div class="cube1"></div>
            <div class="cube2"></div>
            <div class="cube3"></div>
            <div class="cube4"></div>
        </div>
    </div>
</div>
<div id="page" class="site">
    <div class="site-inner">
        <header id="masthead" class="site-header is-sticky">
            <div class="site-header-outer">
                <div class="site-header-inner sticky--pinned">
                    <div class="container">
                        <div class="header-main clearfix">
                            <div class="header-component-outer header-left">
                                <div class="header-component-inner clearfix">
                                    <nav class="site-main-nav clearfix" data-container="#masthead .header-main">
                                        <ul id="menu-primary-navigation" class="main-menu mega-menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children mm-item mm-item-has-sub active mm-popup-wide mm-popup-column-4 mm-popup-max-width mm-item-level-0 menu-item-14">
                                                <a href="{{ __('/') }}"><span class="mm-text">Home</span></a>
                                                <div class="popup">
                                                    <div class="inner"
                                                         style="background-repeat: repeat;background-position: left top;max-width:1000px;"></div>
                                                </div>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mm-item mm-item-has-sub mm-popup-wide mm-popup-column-4 mm-popup-max-width mm-item-level-0 menu-item-15">
                                                <a href="{{ route('shop') }}"><span class="mm-text">Shop</span><span
                                                            class="tip" style="color:#ffffff;background:#d0021b;"><span
                                                                class="tip-arrow"
                                                                style="color:#d0021b;"></span>HOT</span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mm-item mm-item-has-sub mm-popup-narrow mm-item-level-0 menu-item-16">
                                                <a href="{{ route('about') }}"><span
                                                            class="mm-text">Pages</span></a>
                                                <div class="popup">
                                                    <div class="inner" style="">
                                                        <ul class="sub-menu">
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page mm-item mm-item-level-1 menu-item-985"
                                                                data-column="1"><a
                                                                        href="{{ route('about') }}"><span
                                                                            class="mm-text">About Us</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page mm-item mm-item-level-1 menu-item-990"
                                                                data-column="1"><a
                                                                        href="pages/our-teams/index.html"><span
                                                                            class="mm-text">Our Teams</span></a></li>
                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page mm-item mm-item-level-1 menu-item-987"
                                                                data-column="1"><a
                                                                        href="pages/contact-us-01/index.html"><span
                                                                            class="mm-text">Contact Us</span></a>
                                                            </li>

                                                            <li class="menu-item menu-item-type-post_type menu-item-object-page mm-item mm-item-level-1 menu-item-989"
                                                                data-column="1"><a
                                                                        href="pages/faqs-page/index.html"><span
                                                                            class="mm-text">FAQs Page</span></a></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom mm-item mm-popup-narrow mm-item-level-0 menu-item-17">
                                                <a href="pages/collections/index.html"><span
                                                            class="mm-text">Collections</span></a></li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mm-item mm-item-has-sub mm-popup-narrow mm-item-level-0 menu-item-18">
                                                <a href="blog/blog-02-columns/index.html"><span
                                                            class="mm-text">Blog</span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom mm-item mm-popup-narrow mm-item-level-0 menu-item-19">
                                                <a href="shop/shop-instagram/index.html"><span
                                                            class="mm-text">New Look</span></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="header-component-outer header-middle">
                                <div class="site-branding">
                                    <a href="{{ __('/') }}" rel="home">
                                        <figure class="logo--normal">
                                            <img src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}"
                                                 alt="Fegor Shoes - For the Smart and Intelligent"/></figure>
                                        <figure class="logo--transparency"><img
                                                    src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}"
                                                    alt="Fegor Shoes - For the Smart and Intelligent"/></figure>
                                    </a>
                                </div>
                            </div>
                            <div class="header-component-outer header-right">
                                <div class="header-component-inner clearfix">
                                    <div class="header_component header_component--link la_compt_iem la_com_action--aside_header ">
                                        <a rel="nofollow" class="component-target" href="javascript:;"><i
                                                    class="dl-icon-menu2"></i></a></div>
                                    <div class="header_component header_component--dropdown-menu la_compt_iem la_com_action--dropdownmenu ">
                                        <a rel="nofollow" class="component-target" href="javascript:;"><i
                                                    class="fa fa-user-circle-o"></i></a>
                                        <ul id="menu-my-account" class="menu">
                                            @if(!Auth::check())
                                                <li id="menu-item-20"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20">
                                                    <a href="{{ __('register') }}">My Account</a>
                                                </li>
                                            @elseif(Auth::user()->role == 'admin')
                                                <li id="menu-item-20"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20">
                                                    <a href="{{ __('dashboard') }}">Dashboard</a>
                                                </li>
                                            @endif
                                            <li id="menu-item-1138"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1138">
                                                <a href="tracking-order/index.html">Tracking Order</a></li>
                                            <li id="menu-item-1139"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1139">
                                                <a href="compare/index.html">Compare</a></li>
                                            <li id="menu-item-1140"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1140">
                                                <a href="wishlist/index.html">Wishlist</a></li>
                                                @if(!Auth::check())
                                                    <li id="menu-item-1141"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1141">
                                                        <a href="my-account/lost-password/index.html">Lost password</a></li>
                                                @endif

                                                @if(Auth::check())
                                                    <li id="menu-item-1141"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1141">
                                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                           document.getElementById('logout-form').submit();">{{ __('Logout') }}</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form></li>
                                                @endif
                                        </ul>
                                    </div>
                                    <div class="header_component header_component--cart la_compt_iem la_com_action--cart ">
                                        <a rel="nofollow" class="component-target" href="cart/index.html"><i
                                                    class="dl-icon-cart4"></i><span
                                                    class="component-target-badget la-cart-count">0</span><span
                                                    class="la-cart-total-price"><span
                                                        class="woocommerce-Price-amount amount"><span
                                                            class="woocommerce-Price-currencySymbol">&#36;</span>0.00</span></span></a>
                                    </div>
                                    <div class="header_component header_component--searchbox la_compt_iem la_com_action--searchbox searchbox__01 ">
                                        <a class="component-target" href="javascript:;"><i class="dl-icon-search1"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="la-header-sticky-height"></div>
            </div>
        </header>
        <!-- #masthead -->
        <aside id="header_aside" class="header--aside">
            <div class="header-aside-wrapper">
                <a class="btn-aside-toggle" href="#"><i class="dl-icon-close"></i></a>
                <div class="header-aside-inner">
                    <div class="header-widget-bottom">
                        <div id="nav_menu-5" class="accordion-menu widget widget_nav_menu">
                            <div class="menu-header-aside-menu-container">
                                <ul id="menu-header-aside-menu" class="menu">
                                    <li id="menu-item-956"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-956"><a
                                                href="{{ route('about') }}">About Fegor Shoes</a></li>
                                    <li id="menu-item-957"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-957"><a
                                                href="#">Help Center</a></li>
                                    <li id="menu-item-958"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-958"><a
                                                href="{{ route('shop') }}">Shop</a></li>
                                    <li id="menu-item-959"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-959"><a
                                                href="#">Blog</a></li>
                                    <li id="menu-item-960"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-960"><a
                                                href="#">New Look</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="text-4" class="font-size-13 element-max-width-280 padding-top-30 widget widget_text">
                            <div class="textwidget"><p><img
                                            src="https://i1.wp.com/airi.la-studioweb.com/wp-content/themes/airi/assets/images/payments2.png?w=1170&amp;zoom=2&amp;ssl=1"
                                            alt="payment" data-recalc-dims="1"/></p>
                                <p>We accept online payments.</p>
                            </div>
                        </div>
                        <div id="text-5" class="font-size-13 margin-bottom-20 widget widget_text">
                            <div class="textwidget"><p><a href="tel:+2349039464953">(+234) 903 943 6495</a><br/>
                                    <a href="mailto:fegor.ruadjere@gmail.com">fegor.ruadjere@gmail.com</a><br/>
                                    Lekki, Lagos.</p>
                            </div>
                        </div>
                        <div id="text-6" class="font-size-13 margin-bottom-10 widget widget_text">
                            <div class="textwidget"><p><a class="text-color-heading" href="https://www.google.com/maps"
                                                          target="_blank" rel="noopener"><u>Google maps</u></a></p>
                            </div>
                        </div>
                        <div id="text-7" class="widget widget_text">
                            <div class="textwidget">
                                <div class="social-media-link style-default"><a href="#" class="facebook"
                                                                                title="Facebook" target="_blank"
                                                                                rel="nofollow"><i
                                                class="fa fa-facebook"></i></a><a href="instagram.com/fegorshoes" class="twitter"
                                                                                  title="Twitter" target="_blank"
                                                                                  rel="nofollow"><i
                                                class="fa fa-instagram"></i></a></div>
                            </div>
                        </div>
                        <div id="text-8" class="font-size-11 widget widget_text">
                            <div class="textwidget"><p>© 2018 Fegorshoes. All rights reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <div class="site-header-mobile">
            <div class="site-header-outer">
                <div class="site-header-inner">
                    <div class="container">
                        <div class="header-main clearfix">
                            <div class="header-component-outer header-component-outer_logo">
                                <div class="site-branding">
                                    <a href="{{ __('/') }}" rel="home">
                                        <figure class="logo--normal"><img
                                                    src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}"
                                                    alt="Airi – Clean, Minimal WooCommerce Theme"/></figure>
                                        <figure class="logo--transparency"><img
                                                    src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}"
                                                    alt="Airi – Clean, Minimal WooCommerce Theme"/></figure>
                                    </a>
                                </div>
                            </div>
                            <div class="header-component-outer header-component-outer_1">
                                <div class="header-component-inner clearfix">
                                    <div class="header_component header_component--searchbox la_compt_iem la_com_action--searchbox searchbox__01 ">
                                        <a class="component-target" href="javascript:;"><i class="dl-icon-search1"></i></a>
                                    </div>
                                    <div class="header_component header_component--primary-menu la_compt_iem la_com_action--primary-menu ">
                                        <a rel="nofollow" class="component-target" href="javascript:;"><i
                                                    class="dl-icon-menu1"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-menu-wrap">
                        <div id="la_mobile_nav" class="dl-menuwrapper">
                            <ul class="dl-menu dl-menuopen">
                                <li id="menu-item-14"
                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-14">
                                    <a href="{{ __('/') }}">Home</a>
                                </li>
                                <li id="menu-item-15"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-15">
                                    <a href="{{ route('shop') }}">Shop</a>
                                </li>
                                <li id="menu-item-16"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-16">
                                    <a href="pages/about-us/index.html">Pages</a>
                                </li>
                                <li id="menu-item-17"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-17"><a
                                            href="pages/collections/index.html">Collections</a></li>
                                <li id="menu-item-18"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-18">
                                    <a href="blog/blog-02-columns/index.html">Blog</a>
                                </li>
                                <li id="menu-item-19"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-19"><a
                                            href="shop/shop-instagram/index.html">New Look</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="la-header-sticky-height-mb"></div>
            </div>
        </div>
    </div>
</div>