@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif


    <section id="section_page_header" class="wpb_row section-page-header">
        <div class="container">
            <div class="page-header-inner">
                <div class="row">
                    <div class="col-xs-12">
                        <header><h1 class="page-title">About Us</h1></header>
                        <div class="la-breadcrumbs"><div>
                                <div class="la-breadcrumb-content">
                                    <div class="la-breadcrumb-wrap"><div class="la-breadcrumb-item"><a href="../../index.html" class="la-breadcrumb-item-link is-home" rel="home" title="Home">Home</a></div>
                                        <div class="la-breadcrumb-item"><div class="la-breadcrumb-item-sep">/</div></div> <div class="la-breadcrumb-item"><a href="../index.html" class="la-breadcrumb-item-link" rel="tag" title="Pages">Pages</a></div>
                                        <div class="la-breadcrumb-item"><div class="la-breadcrumb-item-sep">/</div></div> <div class="la-breadcrumb-item"><span class="la-breadcrumb-item-target">About Us</span></div>
                                    </div>
                                </div></div>
                        </div>                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- #page_header -->
    <div id="main" class="site-main">
        <div class="container">
            <div class="row">
                <main id="site-content" class="col-md-12 col-xs-12 site-content">
                    <div class="site-content-inner">


                        <div class="page-content">
                            <div class="not-active-fullpage"><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb55ae6db4e3" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6db4e3'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;xlg:120px;lg:80px;md:50px;xs:0px;&quot;}' ></div>
                                            </div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid row--maxwidth-1440 vc_custom_1536941754577 vc_row-has-fill la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner vc_custom_1538017460500"><div class="wpb_wrapper"></div></div></div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner vc_custom_1536942001135"><div class="wpb_wrapper"><div id="la_divider5bb55ae6db8fb" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6db8fb'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:70px;&quot;}' ></div>

                                                <div  class="wpb_single_image wpb_content_element vc_align_left   image__maxiwidth margin-bottom-0">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="185" height="168" src="{{URL::asset('assets/wp-content/themes/airi/assets/images/fegor_logo.png')}}" class="vc_single_image-img attachment-full" alt="" /></div>
                                                    </figure>
                                                </div>
                                                <div id="la_divider5bb55ae6dc01e" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6dc01e'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:30px;&quot;}' ></div>
                                                <div id="la_heading_5bb55ae6dc113" class="la-headings text-left"><div class="js-el subheading-tag la-unit-responsive three-font-family" style="color:#282828"  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb55ae6dc113 .subheading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:16px;&quot;,&quot;line-height&quot;:&quot;lg:24px;&quot;}' >
                                                        <p>We believe in a world where you have total freedom to be you, without judgement. To experiment. To express yourself. To be brave and grab life as the extraordinary adventure it is. So we make sure everyone has an equal chance to discover all the amazing things they’re capable of – no matter who they are, where they’re from or what looks they like to boss. We exist to give you the confidence to be whoever you want to be.</p>
                                                    </div></div>
                                                <div id="la_divider5bb55ae6dc20d" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6dc20d'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:110px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb55ae6dc4c1" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6dc4c1'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:60px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-7"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_heading_5bb55ae6dc7b2" class="la-headings text-left"><h2 class="js-el heading-tag la-unit-responsive letter-spacing--2 padding-bottom-20" style=""  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb55ae6dc7b2 .heading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;xlg:36px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >Fegor Shoes Online Store</h2>
                                                    <div class="js-el subheading-tag la-unit-responsive" style="color:#5d5d5d" >
                                                        <p>You know that satisfying feeling when you stumble across an incredible vintage boutique, or uncover an amazing independent brand, before everyone else? Yeah, we love that too. That’s why we created FegorShoes Marketplace in 2010. The team seeks out the best fashion start-ups and it’s now home to more than 700 boutiques – so you can shop one-of-a-kind finds all the time.</p>
                                                    </div></div>
                                                <div id="la_divider5bb55ae6dc8bb" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6dc8bb'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:30px;&quot;}' ></div>

                                                <div  class="wpb_single_image wpb_content_element vc_align_left   image__maxiwidth">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="418" height="91" src="{{URL::asset('assets/wp-content/uploads/2018/09/about-signature.png')}}" class="vc_single_image-img attachment-full" alt="" /></div>
                                                    </figure>
                                                </div>
                                            </div></div></div><div class="wpb_column vc_column_container vc_col-sm-5"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_left">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="470" height="445" src="{{URL::asset('assets/images/IMG_1411.JPG')}}" class="vc_single_image-img attachment-full" alt="" /></div>
                                                    </figure>
                                                </div>
                                            </div></div></div></div><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_divider5bb55ae6dd7b5" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6dd7b5'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:85px;md:50px;xs:0px;&quot;}' ></div>
                                            </div></div></div></div><div class="vc_row wpb_row vc_row-fluid la_fp_slide la_fp_child_section"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_left   la-popup image-with-icon-play icon-play-at-bottom-right">

                                                    <figure class="wpb_wrapper vc_figure">
                                                            <img width="470" height="451" src="{{URL::asset('assets/images/IMG_1413.JPG')}}" class="vc_single_image-img attachment-full" alt="" />
                                                    </figure>
                                                </div>
                                            </div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div id="la_heading_5bb55ae6de114" class="la-headings text-left"><h2 class="js-el heading-tag la-unit-responsive letter-spacing--2 padding-bottom-20" style=""  data-la_component="UnitResponsive"  data-el_target='#la_heading_5bb55ae6de114 .heading-tag'  data-el_media_sizes='{&quot;font-size&quot;:&quot;xlg:36px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >Why Work With Us ?</h2><div class="js-el subheading-tag la-unit-responsive" style="color:#5d5d5d" >
                                                        <p>We thrive in a culture of restlessness and innovation, which is why we are always on the lookout for passionate and talented people to join the team – people who share our vision, who can make a difference and who want to be part of something big (and fancy picking up a few cheeky samples on the way too).</p>
                                                    </div></div>
                                                <div id="la_divider5bb55ae6de223" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6de223'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:20px;&quot;}' ></div>
                                                <div id="la_icon_boxes_5bb55ae6de314" class="la-sc-icon-boxes wpb_content_element icon-type-normal icon-pos-default icon-style-simple ib-link-none margin-bottom-30">
                                                    <div class="icon-boxes-inner"><div class="box-heading-top"><div class="box-icon-heading"><div class="box-icon-inner type-icon"><div class="wrap-icon"><div class="box-icon box-icon-style-simple"><span><i class="la-icon clothes_glasses"></i></span></div></div></div></div><div class="box-heading"><div class="h5 js-el la-unit-responsive icon-heading font-weight-400" style="color:#5d5d5d"  data-la_component="UnitResponsive"  data-el_target='#la_icon_boxes_5bb55ae6de314 .icon-heading'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:14px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >Durable</div></div></div><div class="box-contents"></div></div>
                                                </div>
                                                <style data-la_component="InsertCustomCSS">#la_icon_boxes_5bb55ae6de314.la-sc-icon-boxes .wrap-icon .box-icon span{line-height:30px;font-size:30px;width:30px;height:30px;color:#cf987e}#la_icon_boxes_5bb55ae6de314.icon-type-normal:hover .wrap-icon .box-icon span{color:#cf987e}</style>
                                                <div id="la_icon_boxes_5bb55ae6de41f" class="la-sc-icon-boxes wpb_content_element icon-type-normal icon-pos-default icon-style-simple ib-link-none margin-bottom-30">
                                                    <div class="icon-boxes-inner"><div class="box-heading-top"><div class="box-icon-heading"><div class="box-icon-inner type-icon"><div class="wrap-icon"><div class="box-icon box-icon-style-simple"><span><i class="la-icon clothes_ring"></i></span></div></div></div></div><div class="box-heading"><div class="h5 js-el la-unit-responsive icon-heading font-weight-400" style="color:#5d5d5d"  data-la_component="UnitResponsive"  data-el_target='#la_icon_boxes_5bb55ae6de41f .icon-heading'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:14px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >Long Lasting</div></div></div><div class="box-contents"></div></div>
                                                </div>
                                                <style data-la_component="InsertCustomCSS">#la_icon_boxes_5bb55ae6de41f.la-sc-icon-boxes .wrap-icon .box-icon span{line-height:30px;font-size:30px;width:30px;height:30px;color:#cf987e}#la_icon_boxes_5bb55ae6de41f.icon-type-normal:hover .wrap-icon .box-icon span{color:#cf987e}</style>
                                                <div id="la_icon_boxes_5bb55ae6de514" class="la-sc-icon-boxes wpb_content_element icon-type-normal icon-pos-default icon-style-simple ib-link-none">
                                                    <div class="icon-boxes-inner"><div class="box-heading-top"><div class="box-icon-heading"><div class="box-icon-inner type-icon"><div class="wrap-icon"><div class="box-icon box-icon-style-simple"><span><i class="la-icon clothes_iron"></i></span></div></div></div></div><div class="box-heading"><div class="h5 js-el la-unit-responsive icon-heading font-weight-400" style="color:#5d5d5d"  data-la_component="UnitResponsive"  data-el_target='#la_icon_boxes_5bb55ae6de514 .icon-heading'  data-el_media_sizes='{&quot;font-size&quot;:&quot;lg:14px;&quot;,&quot;line-height&quot;:&quot;&quot;}' >Quick Delivery</div></div></div><div class="box-contents"></div></div>
                                                </div>
                                                <style data-la_component="InsertCustomCSS">#la_icon_boxes_5bb55ae6de514.la-sc-icon-boxes .wrap-icon .box-icon span{line-height:30px;font-size:30px;width:30px;height:30px;color:#cf987e}#la_icon_boxes_5bb55ae6de514.icon-type-normal:hover .wrap-icon .box-icon span{color:#cf987e}</style>
                                                <div id="la_divider5bb55ae6de619" class="js-el la-divider la-unit-responsive" data-la_component="UnitResponsive"  data-el_target='#la_divider5bb55ae6de619'  data-el_media_sizes='{&quot;padding-top&quot;:&quot;lg:10px;&quot;}' ></div>
                                                <div id="la_heading_5bb55ae6de6b3" class="la-headings text-left"><div class="js-el subheading-tag la-unit-responsive element-max-width-600" style="color:#5d5d5d" ><p>To put it simply, if you’re passionate, innovative and creative, we want to hear from you; wherever you’re from.</p>
                                                    </div></div>
                                            </div></div></div></div>

                            </div>                    </div>

                    </div>
                </main>
                <!-- #site-content -->
            </div>
        </div>
    </div>


@endsection