@extends('layouts.dash-app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="wrapper">
        <div class="container-fluid"><!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group float-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Fegor shoes</a></li>
                                <li class="breadcrumb-item active">Newsletter</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Newsletter</h4></div>
                </div>
            </div><!-- end page title end breadcrumb -->

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body"><h4 class="mt-0 header-title">Newsletter</h4>
                            <p class="text-muted m-b-30 font-14">Send newsletters to all your subscribers</p>
                            <div class="summernote">Hello Fegor Shoes Subscribers</div>
                        </div>
                    </div>
                </div><!-- end col --></div>

@endsection