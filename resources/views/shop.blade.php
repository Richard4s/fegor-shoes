@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <style>
        #page.site {
            /*Uncomment later*/
            min-height: 300px;
            position: relative;
        }
    </style>

    <section id="section_page_header" class="wpb_row section-page-header">
        <div class="container">
            <div class="page-header-inner">
                <div class="row">
                    <div class="col-xs-12">
                        <header><h1 class="page-title">Shop Pages</h1></header>
                        <div class="la-breadcrumbs"><div>
                                <div class="la-breadcrumb-content">
                                    <div class="la-breadcrumb-wrap"><div class="la-breadcrumb-item"><a href="../index.html" class="la-breadcrumb-item-link is-home" rel="home" title="Home">Home</a></div>
                                        <div class="la-breadcrumb-item"><div class="la-breadcrumb-item-sep">/</div></div> <div class="la-breadcrumb-item"><span class="la-breadcrumb-item-target">Shop Pages</span></div>
                                    </div>
                                </div></div>
                        </div>                </div>
                </div>
            </div>
        </div>
    </section>

    <div id="main" class="site-main"><div class="container"><div class="row"><main id="site-content" class="col-md-9 col-xs-12 site-content"><div class="site-content-inner"><div class="page-content">
                            <div class="wc-toolbar-container has-adv-filters">
                                <div class="wc-toolbar wc-toolbar-top clearfix">
                                    <div class="wc-toolbar-left">
                                        <p class="woocommerce-result-count">
                                            Showing 1&ndash;12 of 42 results</p>
                                        <div class="wc-view-count">
                                            <p>Show</p>
                                            <ul><li
                                                ><a href="index1853.html?per_page=6">6</a></li>
                                                <li
                                                        class="active"><a href="index1c07.html?per_page=12">12</a></li>
                                                <li
                                                ><a href="indexadcc.html?per_page=15">15</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wc-toolbar-right">
                                        <div class="btn-advanced-shop-filter">
                                            <span>Filters</span><i></i>
                                        </div>
                                        <div class="wc-ordering">
                                            <p><span>Sort by</span></p>
                                            <ul>
                                                <li class="active"><a href="index5156.html?orderby=popularity">Sort by popularity</a></li>
                                                <li><a href="indexa0bc.html?orderby=rating">Sort by average rating</a></li>
                                                <li><a href="index729c.html?orderby=date">Sort by newness</a></li>
                                                <li><a href="index28f0.html?orderby=price">Sort by price: low to high</a></li>
                                                <li><a href="index6a50.html?orderby=price-desc">Sort by price: high to low</a></li>
                                            </ul>
                                        </div>            </div>
                                </div><!-- .wc-toolbar -->

                                <div class="clearfix"></div>
                                <div class="la-advanced-product-filters clearfix">
                                    <div class="sidebar-inner clearfix">
                                        <div id="la-price-filter-list-2" class="col-xs-12 col-sm-3 widget woocommerce la-price-filter-list"><h3 class="widget-title"><span>Price</span></h3>		<div class="textwidget">
                                                <ul><li class=""><a href="index9c63.html?min_price=20&amp;max_price=40"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>20.00</span><span> - </span><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>40.00</span></a></li> <li class=""><a href="indexe460.html?min_price=40&amp;max_price=50"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>40.00</span><span> - </span><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>50.00</span></a></li> <li class=""><a href="index295a.html?min_price=50&amp;max_price=60"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>50.00</span><span> - </span><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>60.00</span></a></li> <li class=""><a href="indexb41b.html?min_price=60&amp;max_price=80"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>60.00</span><span> - </span><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>80.00</span></a></li> <li class=""><a href="index12f7.html?min_price=80&amp;max_price=100"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>80.00</span><span> - </span><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>100.00</span></a></li> <li class=""><a href="indexfe14.html?min_price=100&amp;max_price=159"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>100.00</span><span> + </span></a></li></ul>		</div>
                                        </div>
                                    </div>
                                    <a class="close-advanced-product-filters hidden visible-xs" href="javascript:;" rel="nofollow"><i class="dl-icon-close"></i></a>
                                </div>
                            </div>

                            <div id="la_shop_products" class="la-shop-products">
                                <div class="la-ajax-shop-loading">
                                    <div class="la-ajax-loading-outer">
                                        <div class="la-loader spinner3">
                                            <div class="dot1"></div>
                                            <div class="dot2"></div>
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                            <div class="cube1"></div>
                                            <div class="cube2"></div>
                                            <div class="cube3"></div>
                                            <div class="cube4"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="product-categories-wrapper">
                                    <ul class="catalog-grid-1 products grid-items grid-space-default xxl-block-grid-4 xl-block-grid-3 lg-block-grid-3 md-block-grid-2 sm-block-grid-1 block-grid-1"></ul></div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="products products-grid grid-space-default products-grid-1 grid-items xxl-block-grid-4 xl-block-grid-3 lg-block-grid-3 md-block-grid-2 sm-block-grid-1 block-grid-1" data-grid_layout="products-grid-1" data-item_selector=".product_item" data-item_margin="0" data-container-width="1170" data-item-width="270" data-item-height="390" data-md-col="3" data-sm-col="2" data-xs-col="1" data-mb-col="1" data-la_component="[]" data-la-effect="sequencefade" data-page_num="1" data-page_num_max="4" data-navSelector=".la-shop-products .la-pagination" data-nextSelector=".la-shop-products .la-pagination a.next">
                                            @foreach($productInfo as $product)
                                                <li class="product_item grid-item product post-897 type-product status-publish has-post-thumbnail product_cat-demo-02-sale-off product_cat-demo-02-top-sale product_cat-fashions product_tag-fashion first instock shipping-taxable purchasable product-type-simple thumb-has-effect prod-rating-on prod-has-rating" data-width="1" data-height="1">
                                                    <div class="product_item--inner">
                                                        <div class="product_item--thumbnail">
                                                            <div class="product_item--thumbnail-holder">
                                                                <a href="../product/product-name-20/index.html" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                    <img width="500" height="575" src="{{ URL::asset('assets/images/').'/'.$product->productImage }}" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="" />
                                                                    <div class="wp-alt-image" data-background-image="{{ URL::asset('assets/images/').'/'.$product->productImage }}" style="background-image: url({{ URL::asset('assets/images/').'/'.$product->secondImage }});"></div>
                                                                    <div class="item--overlay"></div>
                                                                </a>
                                                            </div>
                                                            <div class="product_item_thumbnail_action product_item--action">
                                                                <div class="wrap-addto">
                                                                    <a href="index50f7.html?add-to-cart=897" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="897" data-product_sku="REF. LA-897" aria-label="Add &ldquo;Limited edition v-neck t-shirt&rdquo; to your cart" rel="nofollow" data-product_title="{{ $product->productName }}">
                                                                        <span title="Add to cart">Add to cart</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product_item--info">
                                                            <div class="product_item--info-inner">
                                                                <h3 class="product_item--title"><a href="../product/product-name-20/index.html">{{ $product->productName }}</a></h3>
                                                                <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₦</span>{{ $product->productPrice }}</span></span>
                                                                <div class="item--excerpt">
                                                                    <p>{{ $product->productDescription }}</p>
                                                                </div>			</div>
                                                            <div class="product_item--action">
                                                                <div class="wrap-addto">
                                                                    <a href="index50f7.html?add-to-cart=897" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="897" data-product_sku="REF. LA-897" aria-label="Add &ldquo;{{ $product->productName }}&rdquo; to your cart" data-product_title="{{ $product->productName }}">
                                                                        <span title="Add to cart">Add to cart</span>
                                                                    </a>
                                                                </div>			</div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div><!-- .col-xs-12 -->
                                </div><!-- .row -->



                                <nav class="la-pagination clearfix">
                                    {{ $productInfo->links() }}
                                    <ul class='page-numbers'>
                                        <li><span aria-current='page' class='page-numbers current'>1</span></li>
                                        <li><a class='page-numbers' href='page/2/index.html'>2</a></li>
                                        <li><a class='page-numbers' href='page/3/index.html'>3</a></li>
                                        <li><a class='page-numbers' href='page/4/index.html'>4</a></li>
                                        <li><a class="next page-numbers" href="page/2/index.html"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div><!-- ./page-content --></div><!-- ./site-content-inner --></main><!-- ./site-content -->    <aside id="sidebar_primary" class="col-md-3 col-xs-12">
                    <div class="sidebar-inner">
                        <div id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories">
                            <h3 class="widget-title"><span>Categories</span></h3>
                            <ul class="product-categories">
                                @foreach($catName as $cat)
                                    <li class="cat-item cat-item-62"><a href="{{ route('shop').'/'.$cat->id }}">{{ $cat->categoryName }}</a></li>
                                @endforeach

                            </ul>
                        </div>


                               </div>
                </aside>
            </div><!-- ./row --></div>

@endsection