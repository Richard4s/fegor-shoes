<?php

namespace Fegorshoes\Http\Controllers;

use Fegorshoes\Dashboard;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Fegorshoes\Category;
use Fegorshoes\Product;
use Fegorshoes\User;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Fegorshoes\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Fegorshoes\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Fegorshoes\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $dashboard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Fegorshoes\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }

    public function manage_category(Request $request) {
        $info = Category::get();
        return view('manage_category', compact('info'));
    }

    public function add_category(Request $request) {
        if($request->all()) {
            Category::create([
               'categoryName' => $request['categoryName']
            ]);

            return redirect()->route('manage_category');
        }

        return view('add_category');
    }

    public function edit_category(Request $request, $id = null) {
        $editInfo = Category::where('id', '=', $id)->first();
        if($request->all()) {
            Category::where('id', '=', $request['id'])->update([
                'categoryName' => $request['categoryName']
            ]);

            return redirect()->route('manage_category');
        }

        return view('edit_category', compact('editInfo'));
    }

    public function delete_category(Request $request) {
        if($request->all()) {
            Category::where('id', '=', $request['id'])->delete();
            return redirect()->route('manage_category');
        }
    }

    public function manage_product(Request $request) {
        $info = Product::get();
        return view('manage_product', compact('info'));
    }

    public function add_product(Request $request) {
        $catName = Category::get();

        if ($request->hasFile('productImage')) {

            if($request->hasFile('secondImage')) {
                $secondImage = $request->file('secondImage')->getClientOriginalName();
                $secondName = $secondImage.'.'.$request->file('secondImage')->getClientOriginalExtension();
                $destinationPath = public_path('/assets/images/');
                $request->file('secondImage')->move($destinationPath, $secondImage);
            }

            $image = $request->file('productImage')->getClientOriginalName();
            $name = $image.'.'.$request->file('productImage')->getClientOriginalExtension();
            $destinationPath = public_path('/assets/images/');
            $request->file('productImage')->move($destinationPath, $image);

            $createProduct = Product::create([
                'productName' => $request['productName'],
                'productCategory' => $request['productCategory'],
                'productImage' => $image,
                'secondImage' => $secondImage,
                'productPrice' => $request['productPrice'],
                'productDescription' => $request['productDescription']
            ]);

            return redirect()->route('manage_product');
        }

        return view('add_product', compact('catName'));
    }

    public function edit_product(Request $request, $id = null)
    {


        if ($request->all()) {
            Product::where('id', '=', $request['id'])->update([
                'productName' => $request['productName'],
                'productCategory' => $request['productCategory'],
                'productPrice' => $request['productPrice'],
                'productDescription' => $request['productDescription']
            ]);

        if ($request->hasFile('productImage')) {

            if ($request->hasFile('secondImage')) {
                $secondImage = $request->file('secondImage')->getClientOriginalName();
                $secondName = $secondImage . '.' . $request->file('secondImage')->getClientOriginalExtension();
                $destinationPath = public_path('/assets/images/');
                $request->file('secondImage')->move($destinationPath, $secondImage);

                Product::where('id', '=', $request['id'])->update([
                    'secondImage' => $secondImage,
                ]);
            }

            $image = $request->file('productImage')->getClientOriginalName();
            $name = $image . '.' . $request->file('productImage')->getClientOriginalExtension();
            $destinationPath = public_path('/assets/images/');
            $request->file('productImage')->move($destinationPath, $image);

            Product::where('id', '=', $request['id'])->update([
                'productImage' => $image,
                'secondImage' => $secondImage,
            ]);

            return redirect()->route('manage_product');
        }
            return redirect()->route('manage_product');
    }

    if(isset($id)) {
        $editInfo = Product::where('id', '=', $id)->first();
        $catName = Category::get();
        $catSelected = DB::table('categories')->selectRaw('categories.categoryName')->whereRaw('categories.id = ?', [$editInfo->productCategory])->limit(1)->pluck('categoryName')->first();
    }


        return view('edit_product', compact('editInfo', 'catName', 'catSelected'));
    }

    public function delete_product(Request $request) {
        if($request->all()) {
            Product::where('id', '=', $request['id'])->delete();
            return redirect()->route('manage_product');
        }
    }

    public function manage_users(Request $request) {
        $info = User::get();
        return view('manage_users', compact('info'));
    }

    public function sendnewsletters(Request $request) {
        return view('sendnewsletters');
    }
}
