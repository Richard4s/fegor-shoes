$(document).ready(function() {

    $('.dropify').dropify();

    var categoryDatatable = $('#category-datatable').DataTable({
        "aLengthMenu": [
            [5, 10, 15, -1],
            [5, 10, 15, "All"]
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 10,
        "language": {
            search: ""
        },
    }).each(function() {
        var categoryDatatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = categoryDatatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = categoryDatatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');


    });

    var productDatatable = $('#product-datatable').DataTable({
        "aLengthMenu": [
            [5, 10, 15, -1],
            [5, 10, 15, "All"]
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 10,
        "language": {
            search: ""
        },
    }).each(function() {
        var productDatatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = productDatatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = productDatatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');


    });

    var userDatatable = $('#user-datatable').DataTable({
        "aLengthMenu": [
            [5, 10, 15, -1],
            [5, 10, 15, "All"]
        ],
        "order": [[0, "desc"]],
        "iDisplayLength": 10,
        "language": {
            search: ""
        },
    }).each(function() {
        var userDatatable = $(this);
        // SEARCH - Add the placeholder for Search and Turn this into in-line form control
        var search_input = userDatatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
        search_input.attr('placeholder', 'Search');
        search_input.removeClass('form-control-sm');
        // LENGTH - Inline-Form control
        var length_sel = userDatatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
        length_sel.removeClass('form-control-sm');


    });

    // $("#category-delete").click(function () {
    //     swal({
    //         title: "Are you sure?",
    //         text: "You won't be able to revert this!",
    //         type: "warning",
    //         showCancelButton: !0,
    //         confirmButtonText: "Yes, delete it!",
    //         cancelButtonText: "No, cancel!",
    //         confirmButtonClass: "btn btn-success",
    //         cancelButtonClass: "btn btn-danger m-l-10",
    //         buttonsStyling: !1
    //     }).then(function () {
    //         swal("Deleted!", "The category has been deleted.", "success")
    //     }, function (t) {
    //         "cancel" === t && swal("Cancelled", "This action has been cancelled", "error")
    //     })
    // });

    $("#category-datatable").on('click', '#category-delete', function() {
        var ID = $(this).closest('tr').find('td').eq(0).text();
        swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger m-l-10",
            buttonsStyling: !1
        }).then(function () {
            swal("Deleted!", "The category has been deleted.", "success");
            $.ajax({
                headers: {
                    'X_CSRF_TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '/delete_category',
                data: {
                    'id': ID,
                },
                success: function (data) {
                    console.log('Successful');
                    window.location.reload();
                },
                error: function (data) {
                    console.log('Unsuccessful' + data);
                    window.location.reload();
                }
            });
        }, function (t) {
            "cancel" === t && swal("Cancelled", "This action has been cancelled", "error")
        })

    });

    $("#product-datatable").on('click', '#product-delete', function() {
        var ID = $(this).closest('tr').find('td').eq(0).text();
        swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            confirmButtonClass: "btn btn-success",
            cancelButtonClass: "btn btn-danger m-l-10",
            buttonsStyling: !1
        }).then(function () {
            swal("Deleted!", "The product has been deleted.", "success");
            $.ajax({
                headers: {
                    'X_CSRF_TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '/delete_product',
                data: {
                    'id': ID,
                },
                success: function (data) {
                    console.log('Successful');
                    window.location.reload();
                },
                error: function (data) {
                    console.log('Unsuccessful' + data);
                    window.location.reload();
                }
            });
        }, function (t) {
            "cancel" === t && swal("Cancelled", "This action has been cancelled", "error")
        })

    });

});