<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/shop', 'ShopController@index')->name('shop');
Route::get('/about', 'HomeController@about')->name('about');

Route::get('/manage_category', 'DashboardController@manage_category')->name('manage_category');
Route::get('/add_category', 'DashboardController@add_category')->name('add_category');
Route::post('/add_category', 'DashboardController@add_category')->name('add_category');
Route::get('/edit_category/{id}', 'DashboardController@edit_category')->name('edit_category');
Route::post('/edit_category', 'DashboardController@edit_category')->name('edit_category');
Route::post('/delete_category', 'DashboardController@delete_category')->name('delete_category');

Route::get('/manage_product', 'DashboardController@manage_product')->name('manage_product');
Route::get('/add_product', 'DashboardController@add_product')->name('add_product');
Route::post('/add_product', 'DashboardController@add_product')->name('add_product');
Route::get('/edit_product/{id}', 'DashboardController@edit_product')->name('edit_product');
Route::post('/edit_product', 'DashboardController@edit_product')->name('edit_product');
Route::post('/delete_product', 'DashboardController@delete_product')->name('delete_product');

Route::get('/manage_users', 'DashboardController@manage_users')->name('manage_users');
Route::get('/sendnewsletters', 'DashboardController@sendnewsletters')->name('sendnewsletters');